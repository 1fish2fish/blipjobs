<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->safeEmail,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Models\JobPosting::class, function (Faker\Generator $faker) {
   return [
       'company'       => $faker->company,
       'position'      => $faker->sentence(rand(1,4)),
       'description'   => implode("<br/>",$faker->paragraphs(rand(6,20))),
       'position_type' => array('full-time','part-time','temp','contract','intern','commision')[rand(0,5)],
       'salary'       => $faker->numberBetween(0,100000),
       'salary_per'   => array('hour','day','month','year')[rand(0,3)],
       'education'     => array('none','high-school','bachelor','master','doctorate')[rand(0,4)],
   ] ;
});
