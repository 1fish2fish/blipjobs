<?php

use Illuminate\Database\Seeder;

use App\Models\Auth\Permission;
use App\Models\Auth\Role;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            [
                'name' => 'super-admin',
                'display_name' => 'Super Admin',
                'description' => 'Super Admin'
            ],
            [
                'name' => 'job-seeker',
                'display_name' => 'Job Seeker',
                'description' => 'User that searches for jobs on the site.'
            ],
            [
                'name' => 'job-poster',
                'display_name' => 'Job Poster',
                'description' => 'User that posts jobs to site.'
            ],
        ];

        foreach($roles as $key => $value) {
            Role::create($value);
        }

    }
}
