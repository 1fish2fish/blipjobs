<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'email' => 'drew.ostrowski@gmail.com',
            'password' => bcrypt('password'),
            'created_at' => \Carbon\Carbon::now()->format('Y-m-d H:m:s')
        ]);


        DB::table('users')->insert([
            'email' => 'job@seeker.com',
            'password' => bcrypt('password'),
            'created_at' => \Carbon\Carbon::now()->format('Y-m-d H:m:s')
        ]);

        DB::table('users')->insert([
            'email' => 'job@poster.com',
            'password' => bcrypt('password'),
            'created_at' => \Carbon\Carbon::now()->format('Y-m-d H:m:s')
        ]);

        DB::table('users')->insert([
            'email' => 'job@seeker2.com',
            'password' => bcrypt('password'),
            'created_at' => \Carbon\Carbon::now()->format('Y-m-d H:m:s')
        ]);

        DB::table('users')->insert([
            'email' => 'job@poster2.com',
            'password' => bcrypt('password'),
            'created_at' => \Carbon\Carbon::now()->format('Y-m-d H:m:s')
        ]);

        $this->call(PermissionTableSeeder::class);

        \App\Models\User::where('email', '=', 'drew.ostrowski@gmail.com')->first()->attachRole(\App\Models\Auth\Role::where('name','super-admin')->first());
        \App\Models\User::where('email', '=', 'job@seeker.com')->first()->attachRole(\App\Models\Auth\Role::where('name','job-seeker')->first());
        \App\Models\User::where('email', '=', 'job@poster.com')->first()->attachRole(\App\Models\Auth\Role::where('name','job-poster')->first());
        \App\Models\User::where('email', '=', 'job@seeker2.com')->first()->attachRole(\App\Models\Auth\Role::where('name','job-seeker')->first());
        \App\Models\User::where('email', '=', 'job@poster2.com')->first()->attachRole(\App\Models\Auth\Role::where('name','job-poster')->first());
    }
}
