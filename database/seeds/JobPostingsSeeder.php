<?php

use Illuminate\Database\Seeder;

class JobPostingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\JobPosting::class,15)
            ->create(['user_id' => \App\Models\User::where('email', '=', 'job@poster.com')->first()->id]);


        factory(App\Models\JobPosting::class,15)
            ->create(['user_id' => \App\Models\User::where('email', '=', 'job@poster2.com')->first()->id]);


    }
}
