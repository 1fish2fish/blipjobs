<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterJobPostingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE `blipjobs`.`job_postings`
                        CHANGE COLUMN `salary` `salary` MEDIUMINT(8) UNSIGNED NULL ,
                        CHANGE COLUMN `salary_per` `salary_per` ENUM('hour', 'day', 'month', 'year') CHARACTER SET 'utf8' NULL ,
                        CHANGE COLUMN `education` `education` ENUM('none', 'high-school', 'bachelor', 'master', 'doctorate') CHARACTER SET 'utf8' NULL ");

        DB::statement('ALTER TABLE job_postings ADD posted TINYINT UNSIGNED NOT NULL');
        DB::statement('ALTER TABLE job_postings ADD url VARCHAR(255) NOT NULL');

        DB::statement('ALTER TABLE job_postings ADD job_source_id INTEGER NOT NULL');
        DB::statement('ALTER TABLE job_postings ADD job_source_key VARCHAR(255) NOT NULL');

        DB::statement('ALTER TABLE job_postings ADD UNIQUE INDEX job_source (job_source_id, job_source_key)');

        DB::statement('ALTER TABLE job_postings ADD location_city VARCHAR(255) NOT NULL');
        DB::statement('ALTER TABLE job_postings ADD location_state CHAR(2) NOT NULL');
        DB::statement('ALTER TABLE job_postings ADD location_zip INTEGER UNSIGNED NOT NULL');

        DB::statement('ALTER TABLE job_postings ADD location_latlon POINT NOT NULL');
        DB::statement('ALTER TABLE job_postings ADD SPATIAL INDEX (location_latlon)');

        DB::statement('INSERT INTO job_sources
                        (name,created_at,updated_at)
                        VALUES
                        ("BlipJobs","' . date("Y-m-d H:i:s") . '","' . date("Y-m-d H:i:s") . '"),
                        ("IndeedAPI","' . date("Y-m-d H:i:s") . '","' . date("Y-m-d H:i:s") . '")');

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        DB::statement('ALTER TABLE job_postings drop location_city');
        DB::statement('ALTER TABLE job_postings drop location_state');
        DB::statement('ALTER TABLE job_postings drop location_zip');
        DB::statement('ALTER TABLE job_postings drop location_latlon');
        DB::statement('ALTER TABLE job_postings drop posted');
        DB::statement('ALTER TABLE job_postings drop url');
        DB::statement('ALTER TABLE job_postings drop job_source_id');
        DB::statement('ALTER TABLE job_postings drop job_source_key');
        DB::statement('DELETE FROM job_sources WHERE name IN ("BlipJobs", "IndeedAPI")');
    }
}
