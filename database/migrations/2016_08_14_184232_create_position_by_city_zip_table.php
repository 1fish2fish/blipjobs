<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreatePositionByCityZipTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('position_city', function (Blueprint $table) {
            $table->increments('id');
            $table->string('city');
            $table->char('state',2);;
            $table->char('country',2);
            $table->integer('population')->unsigned();

            $table->index('population');
            $table->unique(['city','state','country']);
        });


        Schema::create('position_zipcode', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('zipcode')->unsigned();
            $table->string('city');
            $table->char('state',2);

            $table->index('zipcode');
        });

        DB::statement('ALTER TABLE position_city ADD latlon POINT NOT NULL');
        DB::statement('ALTER TABLE position_city ADD SPATIAL INDEX (latlon)');
        DB::statement('ALTER TABLE position_zipcode ADD latlon POINT NOT NULL');
        DB::statement('ALTER TABLE position_zipcode ADD SPATIAL INDEX (latlon)');

        /*

        LOAD DATA LOCAL INFILE "/var/www/blipjobs.com/database/data/cities1000.txt"
        INTO TABLE position_city
        FIELDS TERMINATED BY "\t"
        (@dummy,@city,@dummy,@dummy,@lat,@lon,@dummy,@dummy,country,@dummy,state,@dummy,@dummy,@dummy,population,@dummy,@dummy,@dummy,@dummy)
        SET latlon = POINT(@lat, @lon),
        city = UPPER(@city);

        LOAD DATA LOCAL INFILE "/var/www/blipjobs.com/database/data/us-zipcodes.txt"
        INTO TABLE position_zipcode
        FIELDS TERMINATED BY "\t"
        (@dummy,zipcode,@city,@dummy,state,@dummy,@d,@d,@d,@lat,@lon,@d)
        SET latlon = POINT(@lat, @lon),
        city = UPPER(@city);



         */

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('position_city');
        Schema::drop('position_zipcode');
    }
}
