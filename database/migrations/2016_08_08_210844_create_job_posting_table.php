<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateJobPostingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_postings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('company');
            $table->string('position');
            $table->text('description');
            $table->enum('position_type',['full-time','part-time','temp','contract','intern','commision']);
            $table->mediumInteger('salary')->unsigned();
            $table->enum('salary_per',['hour','day','month','year']);
            $table->enum('education',['none','high-school','bachelor','master','doctorate']);
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')
                ->onUpdate('cascade')->onDelete('cascade');

        });

        Schema::create('job_posting_experience_req', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('job_posting_id')->unsigned();
            $table->string('experience');
            $table->tinyInteger('years');

            $table->foreign('job_posting_id')->references('id')->on('job_postings')
                ->onUpdate('cascade')->onDelete('cascade');
        });

        Schema::create('job_posting_language_req', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('job_posting_id')->unsigned();
            $table->string('language');

            $table->foreign('job_posting_id')->references('id')->on('job_postings')
                ->onUpdate('cascade')->onDelete('cascade');
        });

        Schema::create('job_posting_license_req', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('job_posting_id')->unsigned();
            $table->string('license');

            $table->foreign('job_posting_id')->references('id')->on('job_postings')
                ->onUpdate('cascade')->onDelete('cascade');
        });

        DB::statement('ALTER TABLE job_postings ADD FULLTEXT (company)');
        DB::statement('ALTER TABLE job_postings ADD FULLTEXT (position)');
        DB::statement('ALTER TABLE job_postings ADD FULLTEXT (description)');
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('job_posting_experience_req');
        Schema::drop('job_posting_language_req');
        Schema::drop('job_posting_license_req');
        Schema::drop('job_postings');
    }
}
