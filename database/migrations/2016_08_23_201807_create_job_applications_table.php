<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_applications', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_resume_id')->unsigned();
            $table->integer('user_cover_letter_id')->unsigned()->nullable();
            $table->integer('user_id')->unsigned();
            $table->integer('job_posting_id')->unsigned();
            $table->timestamps();

            $table->unique(['job_posting_id','user_id']);

            $table->foreign('user_resume_id')->references('id')->on('user_resumes')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('user_cover_letter_id')->references('id')->on('user_cover_letters')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('job_posting_id')->references('id')->on('job_postings')
                ->onUpdate('cascade')->onDelete('cascade');
        });

        Schema::create('job_application_stats', function (Blueprint $table) {
            $table->integer('job_posting_id')->unsigned();
            $table->smallInteger('application_count')->unsigned();
            $table->foreign('job_posting_id')->references('id')->on('job_postings')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->unique('job_posting_id');
        });

        DB::connection()->getPdo()->exec('CREATE TRIGGER after_job_applications_insert
                       AFTER INSERT ON job_applications
                       FOR EACH ROW
                       INSERT INTO job_application_stats
                       (job_posting_id,application_count)
                       VALUES
                       (NEW.job_posting_id,1)
                       ON DUPLICATE KEY UPDATE
                       application_count = application_count + 1');

        DB::connection()->getPdo()->exec('CREATE TRIGGER after_job_applications_delete
                       AFTER DELETE ON job_applications
                       FOR EACH ROW
                       UPDATE job_application_stats
                       SET application_count = application_count - 1
                       WHERE job_posting_id = OLD.job_posting_id');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('job_applications');
    }
}
