<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobPositionTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_positions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('indeed_name');
        });

        Schema::create('job_position_job_posting', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('job_posting_id')->unsigned();
            $table->integer('job_position_id')->unsigned();

            $table->unique(['job_posting_id','job_position_id']);

            $table->foreign('job_posting_id')->references('id')->on('job_postings')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->foreign('job_position_id')->references('id')->on('job_positions')
                ->onUpdate('cascade')->onDelete('cascade');
        });

        DB::statement("ALTER TABLE job_postings DROP COLUMN position_type");

        DB::table('job_positions')->insert([
            [
                'name'   => 'Full Time',
                'indeed_name' => 'fulltime'
            ],
            [
                'name'   => 'Part Time',
                'indeed_name' => 'parttime'
            ],
            [
                'name'   => 'Temporary',
                'indeed_name' => 'temporary'
            ],
            [
                'name'   => 'Contract',
                'indeed_name' => 'contract'
            ],
            [
                'name'   => 'Internship',
                'indeed_name' => 'internship'
            ],
        ]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('job_position_job_posting');
        Schema::drop('job_positions');

        DB::statement("ALTER TABLE job_postings ADD COLUMN position_type ENUM('full-time','part-time','temp','contract','intern','commision') NOT NULL");
    }
}
