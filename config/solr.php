<?php

return [
    'solr_host' => env('SOLR_HOST', '127.0.0.1'),
    'solr_port' => env('SOLR_PORT', '8983'),
    'solr_path' => env('SOLR_PATH', '/solr/blip_core/'),
];