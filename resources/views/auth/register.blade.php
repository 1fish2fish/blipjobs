@extends('layouts.default')

@section('title', 'Register New User')
@section('metaDescription', 'Blip Jobs is a job search and posting website.  The Site is here to help job seekers to find
employment.  Employers may also use the site to post jobs.  It uses many other sites to aggregate job data.  It also has
functionality to assist users in organizing their job search.')
@section('siteBackground', 'home-background-logo')

@section('content')
<div class="container absolute-center-container">
    @include('partials.messages')

    <div class="panel transparent-panel">
        <div class="panel-heading">
            <ul class="nav nav-pills">
                <li class="active"><a class="register-role" href="#" data-toggle="tab" data-value="job-seeker">Job Seeker</a></li>
                <li><a class="register-role" href="#" data-toggle="tab" data-value="job-poster">Job Poster</a></li>
                </li>
            </ul>
        </div>
        <div class="panel-body">

            <div class="row">
                <div class="col-sm-8 col-sm-offset-2 col-xs-12 text-center">
                    <h1 class="register-form-header">Register Job Seeker</h1>
                </div>
            </div>
            <form class="form-horizontal" role="form" method="POST" action="{{ url('/register') }}">
                {{ csrf_field() }}


                <div class="row">
                    <div class="col-sm-8 col-sm-offset-2 col-xs-12 margin-bottom{{ $errors->has('email') ? ' has-error' : '' }}">
                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email Address" tabindex="1" autofocus>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-8 col-sm-offset-2 col-xs-12 margin-bottom{{ $errors->has('password') ? ' has-error' : '' }}">
                        <input id="password" type="password" class="form-control" name="password" placeholder="Password" tabindex="2">
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-8 col-sm-offset-2 col-xs-12 margin-bottom{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Confirm Password" tabindex="3">
                    </div>
                </div>

                <input id="register-role-input" type="hidden" name="role" value="job-seeker">

                <div class="row">
                    <div class="col-sm-8 col-sm-offset-2 col-xs-12 margin-bottom text-center">
                        <button type="submit" class="btn btn-primary" tabindex="4">Register</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

</div>
@endsection
