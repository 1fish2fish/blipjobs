@extends('layouts.default')

@section('title', 'Reset Password')
@section('metaDescription', 'Blip Jobs is a job search and posting website.  The Site is here to help job seekers to find
employment.  Employers may also use the site to post jobs.  It uses many other sites to aggregate job data.  It also has
functionality to assist users in organizing their job search.')
@section('siteBackground', 'home-background-logo')

@section('content')

<div class="container absolute-center-container">
    <div class="row">
        <div class="col-sm-8 col-sm-offset-2">


            @if ($errors->has('email') | $errors->has('password') | ($errors->has('password_confirmation')))
                <div class="alert alert-danger alert-dismissable" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h2>Reset Failed</h2>
                    @if ($errors->has('email'))
                        {{ $errors->first('email') }}<br/>
                    @endif

                    @if ($errors->has('password'))
                        {{ $errors->first('password') }}<br/>
                    @endif

                    @if ($errors->has('password_confirmation'))
                        {{ $errors->first('password_confirmation') }}<br/>
                    @endif
                </div>
            @endif

            <h1>Reset Password</h1>
            <form class="form-horizontal" role="form" method="POST" action="{{ url('/password/reset') }}">
                {{ csrf_field() }}

                <input type="hidden" name="token" value="{{ $token }}">
                <input id="email" type="email" class="form-control margin-bottom" name="email" value="{{ $email or old('email') }}" placeholder="Email Address">
                <input id="password" type="password" class="form-control  margin-bottom" name="password" placeholder="New Password">
                <input id="password-confirm" type="password" class="form-control  margin-bottom" name="password_confirmation" placeholder="Confirm New Password">
                <button type="submit" class="btn btn-primary">
                    <i class="fa fa-btn fa-refresh"></i> Reset Password
                </button>
            </form>
        </div>
    </div>
</div>
@endsection
