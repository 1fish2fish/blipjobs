@extends('layouts.default')

@section('title', 'Reset Password Request')
@section('metaDescription', 'Blip Jobs is a job search and posting website.  The Site is here to help job seekers to find
employment.  Employers may also use the site to post jobs.  It uses many other sites to aggregate job data.  It also has
functionality to assist users in organizing their job search.')
@section('siteBackground', 'home-background-logo')

<!-- Main Content -->
@section('content')

    <div class="container absolute-center-container">
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif

        <form class="form-horizontal" role="form" method="POST" action="{{ url('/password/email') }}">
            {{ csrf_field() }}


            <div class="col-sm-8 col-sm-offset-2">
                <h1>Reset Password</h1>
            </div>
            <div class="col-sm-8 col-sm-offset-2 margin-bottom">
                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email Address" tabindex="1" autofocus>

                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>

            <div class="col-sm-8 col-sm-offset-2">
                <button type="submit" class="btn btn-primary">
                    <i class="fa fa-btn fa-envelope"></i> Send Password Reset Link
                </button>
            </div>
        </form>
    </div>
@endsection
