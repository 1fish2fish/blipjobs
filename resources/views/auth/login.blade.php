@extends('layouts.default')

@section('title', 'Login')
@section('metaDescription', 'Blip Jobs is a job search and posting website.  The Site is here to help job seekers to find
employment.  Employers may also use the site to post jobs.  It uses many other sites to aggregate job data.  It also has
functionality to assist users in organizing their job search.')
@section('siteBackground', 'home-background-logo')

@section('content')
<div id="login" class="container absolute-center-container">

    @if ($errors->has('email') | $errors->has('password'))

        <div class="alert alert-danger alert-dismissable" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h2>Login Failed</h2>
            @if ($errors->has('email'))
                {{ $errors->first('email') }}<br/>
            @endif

            @if ($errors->has('password'))
                {{ $errors->first('password') }}
            @endif

        </div>

    @endif
    <div class="panel transparent-panel">
        <div class="panel-body">
            <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
                {{ csrf_field() }}

                <div class="row">
                    <div class="col-sm-5">
                        <h1>Login</h1>
                    </div>
                </div>

                <div class="row">

                    <div class="col-sm-5{{ $errors->has('email') ? ' has-error' : '' }} xs-margin-bottom">
                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email Address" tabindex="1" autofocus>
                    </div>

                    <div class="col-sm-5{{ $errors->has('password') ? ' has-error' : '' }}">
                        <input id="password" type="password" class="form-control" name="password" placeholder="Password" tabindex="2">

                        <a class="btn" href="{{ url('/password/reset') }}">Forgot Your Password?</a>
                    </div>

                    <div class="col-sm-2 xs-center">
                        <button type="submit" class="btn btn-primary">
                            <i class="fa fa-btn fa-sign-in"></i> Login
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
