@extends('layouts.errors')

@section('title', '403 Forbidden')
@section('metaDescription', '403 Forbidden Page, Not allowed access.')

@section('content')

    <div class="absolute-center-container">
        <h1 style="color:red">403 Forbidden.</h1>
        <img src="/img/funny-Consuela-Family-Guy-No-No_large.jpg">
    </div>
@endsection