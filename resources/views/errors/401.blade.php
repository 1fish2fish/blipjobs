@extends('layouts.errors')

@section('title', '401 Unauthorized')
@section('metaDescription', '401 Unauthorized, Either you need to login or you are the wrong user.')

@section('content')

    <div class="absolute-center-container">
        <h1 style="color:red">401 Unauthorized.</h1>
        <img src="/img/funny-Consuela-Family-Guy-No-No_large.jpg">
        <p style="color:red">Either you need to login or you are the wrong user.</p>
    </div>
@endsection