Dear Hiring Manager,<br/>
<p>
    Blipjobs.com has a candidate that is interested in the position {{ $jobApp->jobPostings->position }} at
    {{ $jobApp->jobPostings->company }}.  Kindly consider the candidatae for the position.
    Below is the applicants cover letter and attached is their resume.
</p>
<p>
    Thank you for using blipjobs.com
</p>

<h1>COVER LETTER:</h1>
<p>
    {{ $jobApp->userCoverLetters->cover_letter }}
</p>