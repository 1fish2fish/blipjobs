@extends('layouts.default')


@section('title', 'Home Page')
@section('metaDescription', 'Blip Jobs is a job search and posting website.  The Site is here to help job seekers to find
employment.  Employers may also use the site to post jobs.  It uses many other sites to aggregate job data.  It also has
functionality to assist users in organizing their job search.')
@section('siteBackground', 'home-background-logo')

@section('content')

<div class="container">
    <div class="panel panel-default">
        <div class="panel-body">
            <pre>
                <code>
                    {{ $sourceFile }}
                </code>
            </pre>
        </div>
    </div>
</div>

@endsection