@extends('layouts.user')

@section('title', 'Resume Index')
@section('metaDescription', 'List of saved resume for user to insert,update,delete.')
@section('siteBackground', 'home-background-logo')

@section('user-content')

    <div class="panel panel-default">
        <div class="panel-heading">
            Upload Resume
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-6 col-lg-offset-3">
                    <form action="/user/resumes" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}

                        <fieldset class="form-group">
                            <label for="name">Resume Name</label>
                            <input type="text" class="form-control" id="name" name="resume-name" placeholder="Resume Name"
                                   tabindex="1" autofocus>
                        </fieldset>
                        <fieldset class="form-group">
                            <label for="file">Resume File</label>
                            <div class="input-group">
                                <label class="input-group-btn">
                                <span class="btn btn-info">
                                    Browse&hellip; <input type="file" name="resume-file" style="display: none;">
                                </span>
                                </label>
                                <input type="text" class="form-control" readonly>
                            </div>
                        </fieldset>
                        <fieldset class="form-group">
                            <label class="checkbox-inline"><input name="default" type="checkbox" value="true" tabindex="2" checked>Default</label>
                        </fieldset>
                        <button class="btn btn-primary" type="submit" tabindex="3">Upload</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection