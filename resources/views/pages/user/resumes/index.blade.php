@extends('layouts.user')

@section('title', 'Resume Index')
@section('metaDescription', 'List of saved resume for user to insert,update,delete.')
@section('siteBackground', 'home-background-logo')

@section('user-content')

    <div class="panel panel-default resume">
        <div class="panel-heading">Resumes</div>
        <div class="panel-body">
            <div class="loading-overlay">
                <div class="loading-spinner"></div>
            </div>
            <table class="table">
                <thead>
                    <tr>
                        <th>Resume Name</th>
                        <th class="hidden-xs">File Name</th>
                        <th>Default</th>
                        <th class="hidden-sm hidden-xs">Created Date</th>
                        <th>Delete</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($resumes as $resume)
                        <tr id="resume-{{ $resume->id }}">
                            <td>{{ $resume->name }}</td>
                            <td class="hidden-xs">{{ $resume->user_file_name }}</td>
                            <td><a class="btn btn-xs {{ $resume->default?'active-default resume-update-default':'default resume-update-default'}}" data-id="{{ $resume->id }}"><i class="fa fa-check fa-lg"></i></a></td>
                            <td class="hidden-sm hidden-xs">{{ $resume->created_at->format('M j, Y') }}</td>
                            <td><button class="resume-confirm-delete btn-xs btn btn-danger" data-resume-name="{{ $resume->name }}" value="{{ $resume->id }}"><i class="fa fa-close"></i></button></td>
                        </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <td>
                            <a href="/user/resumes/create">
                                <i class="fa fa-plus"></i> Add Resume
                            </a>
                        </td>
                        <td colspan="4"></td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>

    @section('confirmClickClass','resume-delete')
    @section('confirmHeader', 'Delete Resume')
    @section('confirmBody')
        <p>
            Are you sure you want to delete the resume, "<span id="confirm-resume-name"></span>"?
        </p>
    @endsection

    @include('partials.modal.confirmDelete')
@endsection