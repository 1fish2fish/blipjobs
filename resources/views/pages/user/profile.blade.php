@extends('layouts.user')

@section('title', 'User Profile')
@section('metaDescription', 'User can view and edit their profile.')
@section('siteBackground', 'home-background-logo')

@section('user-content')
<div class="panel-default panel">
    <div class="panel-heading">User Information</div>
    <div class="panel-body">
        <div class="col-lg-6 col-lg-offset-3">
            <form action="/user/profile" method="post">
                {{ csrf_field() }}
                <fieldset class="form-group">
                    <label for="first_name">First Name</label>
                    <input type="text" class="form-control" id="first_name" name="first_name"
                           value="{{ Auth::user()->first_name }}" placeholder="First Name"
                           tabindex="1" autofocus>
                </fieldset>
                <fieldset class="form-group">
                    <label for="last_name">Last Name</label>
                    <input type="text" class="form-control" id="last_name" name="last_name"
                           value="{{ Auth::user()->last_name }}" placeholder="Last Name"
                           tabindex="2">
                </fieldset>
                <fieldset class="form-group {{ $errors->get('email') ? ' has-error' : '' }}">
                    <label for="email">Email</label>
                    <input type="email" class="form-control" id="email" name="email"
                           value="{{ Auth::user()->email }}" placeholder="Email Address"
                           tabindex="3">
                    @if($errors->get('email'))
                        @foreach($errors->get('email') as $message)
                            <label class="control-label" for="email">{{ $message }}</label>
                        @endforeach
                    @endif
                </fieldset>
                <fieldset class="form-group">
                    <label for="phone">Phone</label>
                    <input type="number" class="form-control" id="phone" name="phone" value="{{ $contact->phone }}" placeholder="Phone"
                           tabindex="4">
                </fieldset>
                <fieldset class="form-group">
                    <label for="address">Address</label>
                    <input type="text" class="form-control" id="address" name="address"  value="{{ $contact->address }}"
                           placeholder="Address" tabindex="5">
                </fieldset>
                <fieldset class="form-group">
                    <label for="address2">Address2</label>
                    <input type="text" class="form-control" id="address2" name="address2"  value="{{ $contact->address2 }}"
                           placeholder="Address 2"
                           tabindex="6">
                </fieldset>
                <fieldset class="form-group">
                    <label for="city">City</label>
                    <input type="text" class="form-control" id="city" name="city"  value="{{ $contact->city }}" placeholder="City"
                           tabindex="7">
                </fieldset>
                <fieldset class="form-group">
                    <label for="state">State</label>
                    <input type="text" class="form-control" id="state" name="state"  value="{{ $contact->state }}" placeholder="State"
                           tabindex="8">
                </fieldset>
                <fieldset class="form-group">
                    <label for="zip">Zip Code</label>
                    <input type="text" class="form-control" id="zip" name="zip"  value="{{ $contact->zip }}" placeholder="Zip Code"
                           tabindex="9">
                </fieldset>
                <button class="btn btn-primary" type="submit" tabindex="10">Update</button>
            </form>
        </div>
    </div>
</div>

@endsection