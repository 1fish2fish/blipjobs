@extends('layouts.user')

@section('title', 'Cover Letter - ' . $coverLetter->name)
@section('metaDescription', 'Cover Letter View')
@section('siteBackground', 'home-background-logo')

@section('user-content')

    <div class="panel panel-default cover-letter">
        <div class="panel-heading">
            {{ $coverLetter->name }}
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-6 col-lg-offset-3">
                    @foreach(explode("\n",$coverLetter->cover_letter) as $part)
                        <p>{{ $part }}</p>
                    @endforeach
                    <a href="/user/cover-letters/{{ $coverLetter->id }}/edit" class="btn btn-primary btn-edit">
                        Edit
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection