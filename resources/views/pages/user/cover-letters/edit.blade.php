@extends('layouts.user')

@section('title', 'Cover Letter Edit')
@section('metaDescription', 'User edit cover letter')
@section('siteBackground', 'home-background-logo')

@section('user-content')

    <div class="panel panel-default cover-letter">
        <div class="panel-heading">
            Edit Cover Letter
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-6 col-lg-offset-3">
                    <form action="/user/cover-letters/{{ $coverLetter->id }}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="put">

                        <fieldset class="form-group">
                            <label for="name">Cover Letter Name</label>
                            <input type="text" class="form-control" id="name" name="cover-letter-name" placeholder="Cover Letter Name"
                                   tabindex="1" autofocus value="{{ $coverLetter->name }}">
                        </fieldset>
                        <fieldset class="form-group">
                            <label for="cover-letter">Cover Letter Contents</label>
                            <textarea class="form-control" rows="25" id="cover-letter" name="cover-letter" tabindex="2">{{ $coverLetter->cover_letter }}</textarea>
                        </fieldset>
                        <fieldset class="form-group">
                            <label for="default">Default</label>
                            <div id="cover-letter-default-btn-group" class="btn-group btn-toggle" role="group" aria-label="Set To Default Yes/No" data-input-id="default-input">
                                <button class="btn{{ $coverLetter->default == 1 ? " btn-info":"" }}" type="button" tabindex="3" data-value="1">YES</button>
                                <button class="btn{{ $coverLetter->default == 0 ? " btn-info":"" }}" type="button" tabindex="4" data-value="0">NO</button>
                            </div>
                            <input id="default-input" type="hidden" name="default" value="{{ $coverLetter->default }}">
                        </fieldset>
                        <button class="btn btn-primary" type="submit" tabindex="5">Update</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection