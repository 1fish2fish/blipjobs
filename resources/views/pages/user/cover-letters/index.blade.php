@extends('layouts.user')

@section('title', 'Cover Letter')
@section('metaDescription', 'List of saved cover letters for user to insert,update,delete.')
@section('siteBackground', 'home-background-logo')

@section('user-content')

    <div class="panel panel-default cover-letter">
        <div class="panel-heading">Cover Letters</div>
        <div class="panel-body">
            <div class="loading-overlay">
                <div class="loading-spinner"></div>
            </div>
            <table class="table">
                <thead>
                    <tr>
                        <th>Cover Letter Name</th>
                        <th class="hidden-xs">Content Summary</th>
                        <th>Default</th>
                        <th class="hidden-sm hidden-xs">Created Date</th>
                        <th>Edit / Delete</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($coverLetters as $coverLetter)
                        <tr id="cover-letter-{{ $coverLetter->id }}">
                            <td><a href="/user/cover-letters/{{ $coverLetter->id }}">{{ $coverLetter->name }}</a></td>
                            <td class="hidden-xs">{{ substr($coverLetter->cover_letter,0,40) }}...</td>
                            <td><a class="btn btn-xs {{ $coverLetter->default?'active-default cover-letter-update-default':'default cover-letter-update-default'}}" data-id="{{ $coverLetter->id }}"><i class="fa fa-check fa-lg"></i></a></td>
                            <td class="hidden-sm hidden-xs">{{ $coverLetter->created_at->format('M j, Y') }}</td>
                            <td>
                                <a href="/user/cover-letters/{{ $coverLetter->id }}/edit" class="btn-xs btn btn-warning btn-edit">
                                    <i class="fa fa-gear"></i>
                                </a>
                                <button class="cover-letter-confirm-delete btn-xs btn btn-danger" data-cover-letter-name="{{ $coverLetter->name }}" value="{{ $coverLetter->id }}">
                                    <i class="fa fa-close"></i>
                                </button>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <td>
                            <a href="/user/cover-letters/create">
                                <i class="fa fa-plus"></i> Add Cover Letter
                            </a>
                        </td>
                        <td colspan="4"></td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>

    @section('confirmClickClass','cover-letter-delete')
    @section('confirmHeader', 'Delete Cover Letter')
    @section('confirmBody')
        <p>
            Are you sure you want to delete the cover letter, "<span id="confirm-cover-letter-name"></span>"?
        </p>
    @endsection

    @include('partials.modal.confirmDelete')
@endsection