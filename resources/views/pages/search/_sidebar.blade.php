
<div class="col-sm-3 col-lg-2 sidebar job-summary-sidebar">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h2>Search Filters</h2>
        </div>
        <div class="panel-body">
            {!! Form::open(array('route' => 'job-search','method'=>'GET')) !!}
            <div class="form-group-sm">
                <label class="control-label">Job Search:</label>
                {!! Form::text('q', $input['q'], array('placeholder' => '','class' => 'form-control input-sm')) !!}
            </div>
            <div class="form-group-sm">
                <label class="control-label">Location:</label>
                {!! Form::text('l', $input['l'], array('placeholder' => '','class' => 'form-control input-sm', 'id' => 'location')) !!}
            </div>
            <div class="form-group-sm">
                <label class="control-label">Radius:</label>
                {!! Form::select('r',[
                '0'   => 'Exact Location',
                '5'   => '5 miles',
                '10'  => '10 miles',
                '15'  => '15 miles',
                '25'  => '25 miles',
                '50'  => '50 miles',
                '100' => '100 miles'], isset($input['r']) ? $input['r'] : '5' , array('class' => 'form-control ')) !!}
            </div>
            <div class="form-group-sm">
                <label class="control-label">Job Type:</label>
                {!! Form::select('jt',[
                null         => 'All',
                'temporary'  => 'Temporary',
                'parttime'   => 'Part Time',
                'fulltime'   => 'Full Time',
                'internship' => 'Internship',
                'contract'   => 'Contract',
                'commission' => 'Commission'], isset($input['jt']) ? $input['jt'] : null, array('class' => 'form-control ')) !!}
            </div>
            <div class="form-group-sm">
                <label class="control-label">Posting Age:</label>
                {!! Form::select('fromage',[
                'any'    => 'Anytime',
                '15'     => 'within 15 days',
                '7'      => 'within 7 days',
                '3'      => 'within 3 days',
                '1'      => 'since yesterday'], isset($input['fromage']) ? $input['fromage'] : 'any', array('class' => 'form-control ')) !!}
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center submit">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>