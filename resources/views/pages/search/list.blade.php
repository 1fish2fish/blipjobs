@extends('layouts.default')

@section('title', 'Search Results ' . $input['q'] . " " . $input['l'] )
@section('metaDescription', 'Blip Jobs is a job search and posting website.  The Site is here to help job seekers to find
employment.  Employers may also use the site to post jobs.  It uses many other sites to aggregate job data.  It also has
functionality to assist users in organizing their job search.')

@section('content')

<div class="row">
    @include('pages.search._sidebar')
    <div class="col-sm-7 col-lg-8">
        @include('partials.messages')
        <div class="panel panel-default job-summary-panel">
            <div class="panel-heading">

            @if($jobs->count() > 0)
                <div class="row">
                    <div class="col-xs-4"><h1>Results</h1></div>
                    <div class="col-xs-8">
                        <span class="pull-right result-count">
                            {{($jobs->currentPage()-1) * $jobs->perPage() + 1}} - {{$jobs->lastPage() == $jobs->currentPage() ? $jobs->total() : $jobs->currentPage() * $jobs->perPage()}} of {{ $jobs->total() }}
                        </span>
                    </div>
                </div>

                @else
                    <h1>No Results</h1>
                @endif
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1">
                        @foreach($jobs as $job)
                            <div class="job-summary">
                                <div class="row">
                                    @if(Auth::check())
                                    <div class="col-xs-9">
                                    @else
                                    <div class="col-xs-12">
                                    @endif
                                        <h2><a class="job-link" href="{{ $job->url }}" target="{{ $job->target }}" data-status="viewed" data-id="{{ $job->id }}">{{ ucwords(strtolower($job->position)) }}</a></h2>
                                    </div>
                                    @if(Auth::check())
                                    <div class="col-xs-3 dropdown job-status">
                                        <div class="pull-right">
                                            <button id="status-{{ $job->id }}" class="job-status-btn btn btn-sm {{ isset($job->statusStyle) ? $job->statusStyle : "btn-primary" }} dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <span class="job-status-current">{{ isset($job->status) ? $job->status : "New" }}</span>
                                                <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu" aria-labelledby="dropdownMenu2">
                                                <li><a class="job-link" href="#" data-status="applied" data-id="{{ $job->id }}">Applied</a></li>
                                                <li><a class="job-link" href="#" data-status="interview" data-id="{{ $job->id }}">Interviewing</a></li>
                                                <li><a class="job-link" href="#" data-status="offer" data-id="{{ $job->id }}">Job Offer</a></li>
                                                <li><a class="job-link" href="#" data-status="not-interested" data-id="{{ $job->id }}">Not Interested</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    @endif
                                </div>

                                <div class="row">
                                    <div class="col-xs-12">
                                        <p><b>{{ $job->company ? ucwords(strtolower($job->company)) : "Undisclosed" }} - {{ ucwords(strtolower($job->location_city)) . ", " . $job->location_state }}</b></p>
                                        <p>{{ $job->getSnippet() }}</p>
                                        <time>{{ $job->posted_at->format("F j") }}</time><br/>
                                    </div>
                                </div>
                            </div>
                        @endforeach

                        @if($jobs->count() > 0)
                            <div class="text-center">
                                {{ $jobs->appends($input)->links() }}
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection