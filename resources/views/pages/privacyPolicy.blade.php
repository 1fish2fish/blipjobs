@extends('layouts.default')

@section('title', 'Privacy Policy')
@section('metaDescription', 'Blip Jobs Privacy Policy')
@section('siteBackground', 'home-background-logo')

@section('content')

    <div class="container">
        <div id="privacy-policy" class="panel panel-default">
            <div class="panel-body">

                <h1>Privacy Policy</h1>
                <p>
                    This privacy policy has been compiled to better serve those who are concerned with how their
                    'Personally Identifiable Information' (PII) is being used online. PII, as described in US privacy
                    law and
                    information security, is information that can be used on its own or with other information to
                    identify, contact,
                    or locate a single person, or to identify an individual in context. Please read our privacy policy
                    carefully to
                    get a clear understanding of how we collect, use, protect or otherwise handle your Personally
                    Identifiable
                    Information in accordance with our website.
                </p>
                <header>
                    What personal information do we collect from the people that visit our blog, website or app?
                </header>
                <section>
                    <p>When ordering or registering on our site, as appropriate, you may be asked to enter your
                        name, email address, mailing address, phone number or other details to help you with your
                        experience.
                    </p>
                </section>
                <header>When do we collect information?</header>
                <section>
                    <p>We collect information from you when you register on our site, fill out a form or enter
                        information on our site.
                    </p>
                </section>
                <header>How do we use your information?</header>
                <section>
                    <p> We may use the information we collect from you when you register, make a purchase, sign up
                        for our newsletter, respond to a survey or marketing communication, surf the website, or use
                        certain other site
                        features in the following ways:</p>
                    <ul>
                        <li>To personalize your experience and to
                            allow us to deliver the type of content and product offerings in which you are most
                            interested.
                        </li>
                        <li>To send periodic emails regarding your
                            order or other products and services.
                        </li>
                    </ul>
                </section>
                <header>How do we protect your information?</header>
                <section>
                    <p>We do not use vulnerability scanning and/or scanning to PCI standards.</p>

                    <p>We only provide articles and information. We never ask for credit card numbers.</p>

                    <p>We use regular Malware Scanning.</p>

                    <p>Your personal information is contained behind secured networks and is only accessible by a
                        limited number of persons who have special access rights to such systems, and are required to
                        keep the
                        information confidential. In addition, all sensitive/credit information you supply is encrypted
                        via Secure
                        Socket Layer (SSL) technology.
                    </p>

                    <p>We implement a variety of security measures when a user enters, submits, or accesses their
                        information to maintain the safety of your personal information.
                    </p>

                    <p>All transactions are processed through a gateway provider and are not stored or processed on
                        our servers.
                    </p>
                </section>
                <header>Do we use 'cookies'?</header>
                <section>
                    <p>Yes. Cookies are small files that a site or its service provider transfers to your computer's
                        hard drive through your Web browser (if you allow) that enables the site's or service provider's
                        systems to
                        recognize your browser and capture and remember certain information. For instance, we use
                        cookies to help us
                        remember and process the items in your shopping cart. They are also used to help us understand
                        your preferences
                        based on previous or current site activity, which enables us to provide you with improved
                        services. We also use
                        cookies to help us compile aggregate data about site traffic and site interaction so that we can
                        offer better
                        site experiences and tools in the future.
                    </p>
                </section>
                <header>We use cookies to:</header>
                <section>
                    <ul>
                        <li>Understand and save user's preferences for future visits.</li>
                        <li>Compile aggregate data about site
                            traffic and site interactions in order to offer better site experiences and tools in the
                            future. We may also use
                            trusted third-party services that track this information on our behalf.
                        </li>
                    </ul>
                    <p>You can choose to have your computer warn you each time a cookie is being sent, or you
                        can choose to turn off all cookies. You do this through your browser settings. Since browser is
                        a little
                        different, look at your browser's Help Menu to learn the correct way to modify your cookies.</p>
                </section>

                <header>If users disable cookies in their browser:</header>
                <section>
                    <p>If you turn cookies off, some features will be disabled. Some of the features that make your
                        site experience more efficient and may not function properly.
                    </p>
                </section>

                <header>Third-party disclosure</header>
                <br/>

                <header>Do we disclose the information we collect to Third-Parties?</header>
                <section>
                    <p>We sell,trade, or otherwise transfer to outside parties your name, address,city,town, any
                        form or online contact identifier email, name of chat account etc., phone number, cookie number,
                        ip address
                        device serial #, unique device identifier, photo, video or audio file of child Personally
                        Identifiable
                        Information.
                    </p>
                </section>

                <header>Third-party links</header>
                <section>
                    <p>Occasionally, at our discretion, we may include or offer third-party products or services on
                        our website. These third-party sites have separate and independent privacy policies. We
                        therefore have no
                        responsibility or liability for the content and activities of these linked sites. Nonetheless,
                        we seek to
                        protect the integrity of our site and welcome any feedback about these sites.
                    </p>
                </section>

                <header>Google</header>
                <section>
                    <p>Google's advertising requirements can be summed up by Google's Advertising Principles. They
                        are put in place to provide a positive experience for users.
                        <a href="https://support.google.com/adwordspolicy/answer/1316548?hl=en">google's privacy
                            policy</a>

                    <p>We use Google AdSense Advertising on our website.</p>

                    <p>Google, as a third-party vendor, uses cookies to serve ads on our site. Google's use of
                        the DART cookie enables it to serve ads to our users based on previous visits to our site and
                        other sites on the
                        Internet. Users may opt-out of the use of the DART cookie by visiting the Google Ad and Content
                        Network privacy
                        policy.</p>

                    <p>We have implemented the following:</p>
                    <ul>
                        <li>Remarketing with Google AdSense</li>
                        <li>Google Display Network Impression Reporting</li>
                        <li>Demographics and Interests Reporting</li>
                        <li>DoubleClick Platform Integration</li>
                    </ul>
                    <p>We, along with third-party vendors such as Google use first-party cookies (such as the Google
                        Analytics cookies) and third-party cookies (such as the DoubleClick cookie) or other third-party
                        identifiers
                        together to compile data regarding user interactions with ad impressions and other ad service
                        functions as they
                        relate to our website.
                    </p>
                </section>

                <header>Opting out:</header>
                <section>
                    <p>Users can set preferences for how Google advertises to you using the Google Ad Settings page.
                        Alternatively, you can opt out by visiting the Network Advertising Initiative Opt Out page or by
                        using
                        the Google Analytics Opt Out Browser add on.
                    </p>
                </section>

                <header>California Online Privacy Protection Act</header>
                <section>
                    <p>CalOPPA is the first state law in the nation to require commercial websites and online
                        services to post a privacy policy. The law's reach stretches well beyond California to require
                        any person or
                        company in the United States (and conceivably the world) that operates websites collecting
                        Personally
                        Identifiable Information from California consumers to post a conspicuous privacy policy on its
                        website stating
                        exactly the information being collected and those individuals or companies with whom it is being
                        shared.<br/>
                        See more at: <a
                                href="http://consumercal.org/california-online-privacy-protection-act-caloppa/#sthash.0FdRbT51.dpuf">CalOPPA</a>
                    </p>
                </section>
                <header>According to CalOPPA, we agree to the following:</header>
                <section>
                    <p>Users can visit our site anonymously.</p>

                    <p>Once this privacy policy is created, we will add a link to it on our home page or as a
                        minimum, on the first significant page after entering our website.<br></p>

                    <p>Our Privacy Policy link includes the word 'Privacy' and can be easily be found on the page
                        specified above.
                    </p>

                    <p>You will be notified of any Privacy Policy changes:</p>
                    <ul>
                        <li>On our Privacy Policy Page</li>
                    </ul>
                    <p>You can change your personal information:</p>
                    <ul>
                        <li>By logging in to your account</li>
                    </ul>
                </section>

                <header>How does our site handle Do Not Track signals?</header>
                <section>
                    <p>We honor Do Not Track signals and Do Not Track, plant cookies, or use advertising when a Do
                        Not Track (DNT) browser mechanism is in place.
                    </p>
                </section>

                <header>Does our site allow third-party behavioral tracking?</header>
                <section>
                    <p>It's also important to note that we allow third-party behavioral tracking</p>
                </section>

                <header>COPPA (Children Online Privacy Protection Act)</header>
                <section>
                    <p>When it comes to the collection of personal information from children under the age of 13
                        years old, the Children's Online Privacy Protection Act (COPPA) puts parents in control. The
                        Federal Trade
                        Commission, United States' consumer protection agency, enforces the COPPA Rule, which spells out
                        what operators
                        of websites and online services must do to protect children's privacy and safety online.</p>

                    <p>We do not specifically market to children under the age of 13 years old.</p>
                </section>
                <header>Fair Information Practices</header>
                <section>
                    <p>The Fair Information Practices Principles form the backbone of privacy law in the United
                        States and the concepts they include have played a significant role in the development of data
                        protection laws
                        around the globe. Understanding the Fair Information Practice Principles and how they should be
                        implemented is
                        critical to comply with the various privacy laws that protect personal information.</p>
                </section>
                <header>In order to be in line with Fair Information Practices we will take the following
                    responsive action, should a data breach occur:
                </header>
                <section>
                    <p>We will notify you via email within 7 business days</p>

                    <p>We also agree to the Individual Redress Principle which requires that individuals have
                        the right to legally pursue enforceable rights against data collectors and processors who fail
                        to adhere to the
                        law. This principle requires not only that individuals have enforceable rights against data
                        users, but also that
                        individuals have recourse to courts or government agencies to investigate and/or prosecute
                        non-compliance by
                        data processors.
                    </p>
                </section>
                <header>CAN SPAM Act</header>
                <section>
                    <p>The CAN-SPAM Act is a law that sets the rules for commercial email, establishes requirements
                        for commercial messages, gives recipients the right to have emails stopped from being sent to
                        them, and spells
                        out tough penalties for violations.</p>
                </section>
                <header>We collect your email address in order to:</header>
                <section>
                    <ul>
                        <li>Send information, respond to inquiries, and/or other requests or questions</li>
                        <li>Market to our mailing list or continue to send emails to our clients after the original
                            transaction has occurred.
                        </li>
                    </ul>
                </section>
                <header>To be in accordance with CANSPAM, we agree to the following:</header>
                <section>
                    <ul>
                        <li>Not use false or misleading subjects or email addresses.</li>
                        <li>Identify the message as an advertisement in some reasonable way.</li>
                        <li>Include the physical address of our business or site headquarters.</li>
                        <li>Monitor third-party email marketing services for compliance, if one is used.</li>
                        <li>Honor opt-out/unsubscribe requests quickly.</li>
                        <li>Allow users to unsubscribe by using the link at the bottom of each email.</li>
                    </ul>
                    <p>If at any time you would like to unsubscribe from receiving future emails, you
                        can do one of the following and we will promptly remove you from <strong>ALL</strong>
                        correspondence. </p>
                    <ul>
                        <li>email us at unsubscribe@blipjobs.com</li>
                        <li>Follow the instructions at the bottom of each email.</li>
                    </ul>
                </section>

                <header>Contacting Us</header>
                <section>
                    <p>If there are any questions regarding this privacy policy, you may contact us using the
                        information below.</p>

                    <p>BlipJobs.com<br/>
                        120 Agate Ave<br/>
                        Newport Beach, CA 92662<br/>
                        USA<br/>
                        support@blipjobs.com
                    </p>
                </section>
                <br/>
                <br/>

                <p>Last Edited on 2016-07-14</p>
            </div>
        </div>
    </div>

@endsection