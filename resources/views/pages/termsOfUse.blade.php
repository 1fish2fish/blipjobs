@extends('layouts.default')

@section('title', 'Terms Of Use')
@section('metaDescription', 'Terms Of Use')
@section('siteBackground', 'home-background-logo')

@section('content')

    <div class="container">
        <div id="terms-of-use" class="panel panel-default">
            <div class="panel-body">
                <h1>Terms and conditions of use</h1>
                <section class="terms-of-use">
                <header>1. Introduction</header>
                <section>
                    <p>1.1 These terms and conditions shall govern your use of our website.</p>

                    <p>1.2 By using our website, you accept these terms and conditions in full; accordingly, if you
                        disagree
                        with these
                        terms and conditions or any part of these terms and conditions, you must not use our
                        website.</p>

                    <p>1.3 If you register with our website, submit any material to our website or use any of our
                        website
                        services, we will
                        ask you to expressly agree to these terms and conditions.</p>

                    <p>1.4 Our website uses cookies; by using our website or agreeing to these terms and conditions, you
                        consent to our use
                        of cookies in accordance with the terms of our privacy policy.</p>
                </section>

                <header>2. Credit</header>
                <section>
                    <p>2.1 This document was created using a template from SEQ Legal (http://www.seqlegal.com).</p>

                    <p>3. Copyright notice</p>

                    <p>3.1 Copyright (c) 2016 Blip Jobs.</p>

                    <header>3.2 Subject to the express provisions of these terms and conditions:</header>
                    <section>
                        <p>(a) we, together with our licensors, own and control all the copyright and other
                            intellectual
                            property rights in our
                            website and the material on our website; and</p>
                        <p>(b) all the copyright and other intellectual property rights in our website and the material
                            on our
                            website are
                            reserved.</p>
                    </section>
                </section>

                <header>4. Licence to use website</header>
                <section>
                    <header>4.1 You may:</header>
                    <section>
                        <p>(a) view pages from our website in a web browser;</p>

                        <p>(b) download pages from our website for caching in a web browser;</p>

                        <p>(c) print pages from our website;</p>

                        <p>subject to the other provisions of these terms and conditions.</p>
                    </section>

                    <p>4.2 Except as expressly permitted by Section 4.1 or the other provisions of these terms and
                        conditions, you must not
                        download any material from our website or save any such material to your computer.</p>

                    <p>4.3 You may only use our website for your own personal and business purposes, and you must not
                        use
                        our website for
                        any other purposes.</p>

                    <p>4.4 Except as expressly permitted by these terms and conditions, you must not edit or otherwise
                        modify any material
                        on our website.</p>

                    <header>4.5 Unless you own or control the relevant rights in the material, you must not:</header>
                    <section>
                        <p>(a) republish material from our website (including republication on another website);</p>

                        <p>(b) sell, rent or sub-license material from our website;</p>

                        <p>(c) show any material from our website in public;</p>

                        <p>(d) exploit material from our website for a commercial purpose; or</p>

                        <p>(e) redistribute material from our website.</p>
                    </section>

                    <p>4.6 Notwithstanding Section 4.5, you may redistribute our newsletter in print and electronic form
                        to
                        any person.</p>

                    <p>4.7 We reserve the right to restrict access to areas of our website, or indeed our whole website,
                        at
                        our discretion;
                        you must not circumvent or bypass, or attempt to circumvent or bypass, any access restriction
                        measures on our
                        website.</p>
                </section>

                <header>5. Acceptable use</header>
                <section>
                    <header>5.1 You must not:</header>
                    <section>
                        <p>(a) use our website in any way or take any action that causes, or may cause, damage to the
                            website or
                            impairment of
                            the performance, availability or accessibility of the website;</p>

                        <p>(b) use our website in any way that is unlawful, illegal, fraudulent or harmful, or in connection
                            with any unlawful,
                            illegal, fraudulent or harmful purpose or activity;</p>

                        <p>(c) use our website to copy, store, host, transmit, send, use, publish or distribute any material
                            which consists of
                            (or is linked to) any spyware, computer virus, Trojan horse, worm, keystroke logger, rootkit or
                            other malicious
                            computer software;</p>

                        <p>(d) conduct any systematic or automated data collection activities (including without limitation
                            scraping, data
                            mining, data extraction and data harvesting) on or in relation to our website without our
                            express
                            written
                            consent;</p>

                        <p>(e) access or otherwise interact with our website using any robot, spider or other automated
                            means,
                            except for the
                            purpose of search engine indexing;</p>

                        <p>(f) violate the directives set out in the robots.txt file for our website; or</p>

                        <p>(g) use data collected from our website for any direct marketing activity (including without
                            limitation email
                            marketing, SMS marketing, telemarketing and direct mailing).</p>
                    </section>

                    <p>5.2 You must not use data collected from our website to contact individuals, companies or other
                        persons or
                        entities.</p>

                    <p>5.3 You must ensure that all the information you supply to us through our website, or in relation
                        to
                        our website, is
                        true, accurate, current, complete and non-misleading.</p>
                </section>

                <header>6. Registration and accounts</header>
                <section>
                    <p>6.1 You may register for an account with our website by completing and submitting the account
                        registration form on
                        our website, and clicking on the verification link in the email that the website will send to
                        you.</p>

                    <p>6.2 You must not allow any other person to use your account to access the website.</p>

                    <p>6.3 You must notify us in writing immediately if you become aware of any unauthorized use of your
                        account.</p>

                    <p>6.4 You must not use any other person's account to access the website, unless you have that
                        person's
                        express
                        permission to do so.</p>
                </section>

                <header>7. User login details</header>
                <section>
                    <p>7.1 If you register for an account with our website, we will provide you with OR you will be
                        asked to
                        choose a user
                        ID and password.</p>

                    <p>7.2 Your user ID must not be liable to mislead and must comply with the content rules set out in
                        Section 10; you must
                        not use your account or user ID for or in connection with the impersonation of any person.</p>

                    <p>7.3 You must keep your password confidential.</p>

                    <p>7.4 You must notify us in writing immediately if you become aware of any disclosure of your
                        password.</p>

                    <p>7.5 You are responsible for any activity on our website arising out of any failure to keep your
                        password
                        confidential, and may be held liable for any losses arising out of such a failure.</p>
                </section>

                <header>8. Cancellation and suspension of account</header>
                <section>
                    <header>8.1 We may:</header>
                    <section>
                        <p>(a) suspend your account;</p>

                        <p>(b) cancel your account; and/or</p>

                        <p>(c) edit your account details,</p>

                        <p>at any time in our sole discretion without notice or explanation.</p>
                    </section>

                    <p>8.2 You may cancel your account on our website using your account control panel on the
                        website.</p>
                </section>

                <header>9. Your content: license</header>
                <section>
                    <p>9.1 In these terms and conditions, "your content" means all works and materials (including
                        without
                        limitation text,
                        graphics, images, audio material, video material, audio-visual material, scripts, software and
                        files) that you
                        submit to us or our website for storage or publication on, processing by, or transmission via,
                        our
                        website.</p>

                    <p>9.2 You grant to us a worldwide, irrevocable, non-exclusive, royalty-free licence to use,
                        reproduce,
                        store, adapt,
                        publish, translate and distribute your content in any existing or future media.</p>

                    <p>9.3 You grant to us the right to sub-license the rights licensed under Section 9.2.</p>

                    <p>9.4 You grant to us the right to bring an action for infringement of the rights licensed under
                        Section 9.2.</p>

                    <p>9.5 You hereby waive all your moral rights in your content to the maximum extent permitted by
                        applicable law; and you
                        warrant and represent that all other moral rights in your content have been waived to the
                        maximum
                        extent permitted
                        by applicable law.</p>

                    <p>9.6 You may edit your content to the extent permitted using the editing functionality made
                        available
                        on our
                        website.</p>

                    <p>9.7 Without prejudice to our other rights under these terms and conditions, if you breach any
                        provision of these
                        terms and conditions in any way, or if we reasonably suspect that you have breached these terms
                        and
                        conditions in
                        any way, we may delete, unpublish or edit any or all of your content.</p>
                </section>

                <header>10. Your content: rules</header>
                <section>
                    <p>10.1 You warrant and represent that your content will comply with these terms and conditions.</p>

                    <p>10.2 Your content must not be illegal or unlawful, must not infringe any person's legal rights,
                        and
                        must not be
                        capable of giving rise to legal action against any person (in each case in any jurisdiction and
                        under any applicable
                        law).</p>

                    <header>10.3 Your content, and the use of your content by us in accordance with these terms and
                        conditions,
                        must not:</header>
                    <section>
                        <p>(a) be libellous or maliciously false;</p>

                        <p>(b) be obscene or indecent;</p>

                        <p>(c) infringe any copyright, moral right, database right, trade mark right, design right, right in
                            passing off, or
                            other intellectual property right;</p>

                        <p>(d) infringe any right of confidence, right of privacy or right under data protection
                            legislation;</p>

                        <p>(e) constitute negligent advice or contain any negligent statement;</p>

                        <p>(f) constitute an incitement to commit a crime, instructions for the commission of a crime or the
                            promotion of
                            criminal activity;</p>

                        <p>(g) be in contempt of any court, or in breach of any court order;</p>

                        <p>(h) be in breach of racial or religious hatred or discrimination legislation;</p>

                        <p>(i) be blasphemous;</p>

                        <p>(j) be in breach of official secrets legislation;</p>

                        <p>(k) be in breach of any contractual obligation owed to any person;</p>

                        <p>(l) depict violence in an explicit, graphic or gratuitous manner;</p>

                        <p>(m) be pornographic, lewd, suggestive or sexually explicit;</p>

                        <p>(n) be untrue, false, inaccurate or misleading;</p>

                        <p>(o) consist of or contain any instructions, advice or other information which may be acted upon
                            and
                            could, if acted
                            upon, cause illness, injury or death, or any other loss or damage;</p>

                        <p>(p) constitute spam;</p>

                        <p>(q) be offensive, deceptive, fraudulent, threatening, abusive, harassing, anti-social, menacing,
                            hateful,
                            discriminatory or inflammatory; or</p>

                        <p>(r) cause annoyance, inconvenience or needless anxiety to any person.</p>
                    </section>
                </section>

                <header>11. Limited warranties</header>
                <section>
                    <header>11.1 We do not warrant or represent:</header>
                    <section>
                        <p>(a) the completeness or accuracy of the information published on our website;</p>

                        <p>(b) that the material on the website is up to date; or</p>

                        <p>(c) that the website or any service on the website will remain available.</p>
                    </section>

                    <p>11.2 We reserve the right to discontinue or alter any or all of our website services, and to stop
                        publishing our
                        website, at any time in our sole discretion without notice or explanation; and save to the
                        extent
                        expressly provided
                        otherwise in these terms and conditions, you will not be entitled to any compensation or other
                        payment upon the
                        discontinuance or alteration of any website services, or if we stop publishing the website.</p>

                    <p>11.3 To the maximum extent permitted by applicable law and subject to Section 12.1, we exclude
                        all
                        representations
                        and warranties relating to the subject matter of these terms and conditions, our website and the
                        use
                        of our
                        website.</p>
                </section>

                <header>12. Limitations and exclusions of liability</header>
                <section>
                    <header>12.1 Nothing in these terms and conditions will:</header>
                    <section>
                        <p>(a) limit or exclude any liability for death or personal injury resulting from negligence;</p>

                        <p>(b) limit or exclude any liability for fraud or fraudulent misrepresentation;</p>

                        <p>(c) limit any liabilities in any way that is not permitted under applicable law; or</p>

                        <p>(d) exclude any liabilities that may not be excluded under applicable law.</p>
                    </section>

                    <header>12.2 The limitations and exclusions of liability set out in this Section 12 and elsewhere in
                        these
                        terms and
                        conditions:</header>
                    <section>
                        <p>(a) are subject to Section 12.1; and</p>

                        <p>(b) govern all liabilities arising under these terms and conditions or relating to the subject
                            matter
                            of these terms
                            and conditions, including liabilities arising in contract, in tort (including negligence) and
                            for
                            breach of
                            statutory duty, except to the extent expressly provided otherwise in these terms and
                            conditions.</p>
                    </section>

                    <p>12.3 To the extent that our website and the information and services on our website are provided
                        free
                        of charge, we
                        will not be liable for any loss or damage of any nature.</p>

                    <p>12.4 We will not be liable to you in respect of any losses arising out of any event or events
                        beyond
                        our reasonable
                        control.</p>

                    <p>12.5 We will not be liable to you in respect of any business losses, including (without
                        limitation)
                        loss of or damage
                        to profits, income, revenue, use, production, anticipated savings, business, contracts,
                        commercial
                        opportunities or
                        goodwill.</p>

                    <p>12.6 We will not be liable to you in respect of any loss or corruption of any data, database or
                        software.</p>

                    <p>12.7 We will not be liable to you in respect of any special, indirect or consequential loss or
                        damage.</p>

                    <p>12.8 You accept that we have an interest in limiting the personal liability of our officers and
                        employees and, having
                        regard to that interest, you acknowledge that we are a limited liability entity; you agree that
                        you
                        will not bring
                        any claim personally against our officers or employees in respect of any losses you suffer in
                        connection with the
                        website or these terms and conditions (this will not, of course, limit or exclude the liability
                        of
                        the limited
                        liability entity itself for the acts and omissions of our officers and employees).</p>
                </section>

                <header>13. Breaches of these terms and conditions</header>
                <section>
                    <header>13.1 Without prejudice to our other rights under these terms and conditions, if you breach these
                        terms and conditions
                        in any way, or if we reasonably suspect that you have breached these terms and conditions in any
                        way, we may:</header>
                    <section>
                        <p>(a) send you one or more formal warnings;</p>

                        <p>(b) temporarily suspend your access to our website;</p>

                        <p>(c) permanently prohibit you from accessing our website;</p>

                        <p>(d) block computers using your IP address from accessing our website;</p>

                        <p>(e) contact any or all of your internet service providers and request that they block your access
                            to
                            our website;</p>

                        <p>(f) commence legal action against you, whether for breach of contract or otherwise; and/or</p>

                        <p>(g) suspend or delete your account on our website.</p>
                    </section>

                    <p>13.2 Where we suspend or prohibit or block your access to our website or a part of our website,
                        you
                        must not take any
                        action to circumvent such suspension or prohibition or blocking (including without limitation
                        creating and/or using
                        a different account).</p>
                </section>

                <header>14. Variation</header>
                <section>
                    <p>14.1 We may revise these terms and conditions from time to time.</p>

                    <p>14.2 The revised terms and conditions shall apply to the use of our website from the date of
                        publication of the
                        revised terms and conditions on the website, and you hereby waive any right you may otherwise
                        have
                        to be notified
                        of, or to consent to, revisions of these terms and conditions.</p>

                    <p>14.3 If you have given your express agreement to these terms and conditions, we will ask for your
                        express agreement
                        to any revision of these terms and conditions; and if you do not give your express agreement to
                        the
                        revised terms
                        and conditions within such period as we may specify, we will disable or delete your account on
                        the
                        website, and you
                        must stop using the website.</p>
                </section>

                <header>15. Assignment</header>
                <section>
                    <p>15.1 You hereby agree that we may assign, transfer, sub-contract or otherwise deal with our
                        rights
                        and/or obligations
                        under these terms and conditions.</p>

                    <p>15.2 You may not without our prior written consent assign, transfer, sub-contract or otherwise
                        deal
                        with any of your
                        rights and/or obligations under these terms and conditions.</p>
                </section>

                <header>16. Severability</header>
                <section>
                    <p>16.1 If a provision of these terms and conditions is determined by any court or other competent
                        authority to be
                        unlawful and/or unenforceable, the other provisions will continue in effect.</p>

                    <p>16.2 If any unlawful and/or unenforceable provision of these terms and conditions would be lawful
                        or
                        enforceable if
                        part of it were deleted, that part will be deemed to be deleted, and the rest of the provision
                        will
                        continue in
                        effect.</p>
                </section>

                <header>17. Third party rights</header>
                <section>
                    <p>17.1 A contract under these terms and conditions is for our benefit and your benefit, and is not
                        intended to benefit
                        or be enforceable by any third party.</p>

                    <p>17.2 The exercise of the parties' rights under a contract under these terms and conditions is not
                        subject to the
                        consent of any third party.</p>
                </section>

                <header>18. Entire agreement</header>
                <section>
                    <p>18.1 Subject to Section 12.1, these terms and conditions, together with our privacy policy, shall
                        constitute the
                        entire agreement between you and us in relation to your use of our website and shall supersede
                        all
                        previous
                        agreements between you and us in relation to your use of our website.</p>
                </section>

                <header>19. Law and jurisdiction</header>
                <section>
                    <p>19.1 These terms and conditions shall be governed by and construed in accordance with US law.</p>

                    <p>19.2 Any disputes relating to these terms and conditions shall be subject to the exclusive
                        jurisdiction of the
                        court.</p>
                </section>

                <header>20. Our details</header>
                <section>
                    <p>20.1 This website is owned and operated by Blip Jobs</p>

                    <p>20.2 We are registered in Orange County, California USA under registration number 20166447236,
                        and
                        our registered
                        office is at 12 Civic Center Plaza, Room 106 Santa Ana, CA 92702-0238.</p>

                    <p>20.3 Our principal place of business is at 120 Agate Ave. Newport Beach, CA 92662 USA.</p>

                    <header>20.4 You can contact us:</header>
                    <section>
                        <p>(a) by post, using the postal address given above;</p>

                        <p>(b) using our website contact form;</p>

                        <p>(c) by telephone, on the contact number published on our website from time to time; or</p>

                        <p>(d) by email, using the email address published on our website from time to time.</p>
                    </section>
                </section>
            </section>
            </div>
        </div>
    </div>

@endsection