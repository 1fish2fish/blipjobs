@extends('layouts.poster')

@section('title', '')
@section('metaDescription', '')
@section('siteBackground', 'home-background-logo')

@section('poster-content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <h1>Create Job Posting</h1>
        </div>
        <div class="panel-body">
            {!! Form::open(array('route' => 'job.store','method'=>'POST')) !!}
            @include('pages.job._form')
            <div class="row">
                <div class="col-xs-12 text-center">
                    <button type="submit" class="btn btn-primary" tabindex="7">Next</button>
                </div>
            </div>
            {!! Form::close() !!}

        </div>
    </div>
@endsection