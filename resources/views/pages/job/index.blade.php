@extends('layouts.poster')

@section('title', '')
@section('metaDescription', '')
@section('siteBackground', 'home-background-logo')

@section('poster-content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="row">
                <div class="col-xs-8">
                    <h1>Job Posting</h1>
                </div>
                <div class="col-xs-4">
                    <div class="pull-right">
                        @if(! $jobPostings->lastPage() && $jobPostings->currentPage() != 1)
                        {{ ( $jobPostings->currentPage() * $jobPostings->perPage()) - $jobPostings->perPage() + 1 }} -
                        {{  $jobPostings->currentPage() * $jobPostings->perPage() }} of {{ $jobPostings->total() }}
                        @else
                            {{ ( $jobPostings->currentPage() * $jobPostings->perPage()) - $jobPostings->perPage() + 1 }} -
                            {{  $jobPostings->currentPage() * $jobPostings->perPage() - $jobPostings->perPage() + $jobPostings->count() }}
                            of {{ $jobPostings->total() }}
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="panel-body">

        @include('pages.job._table')

        </div>
    </div>
@endsection