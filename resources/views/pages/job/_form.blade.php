<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script type="text/javascript">
    tinymce.init({
        selector: '#job-post-textarea'
    });
</script>


<div class="row">
    <div class="col-lg-6 col-sm-12">
        <div class="form-group">
            <strong>Company:</strong>
            {!! Form::text('company', null, array('placeholder' => '','class' => 'form-control', 'tabindex' => 1, 'autofocus')) !!}
        </div>
    </div>
    <div class="col-lg-6 col-sm-12">
        <strong>Position:</strong>
        <div class="row">
            <div class="col-xs-8">
                <div class="form-group">
                    {!! Form::text('position', null, array('placeholder' => '','class' => 'form-control', 'tabindex' => 2)) !!}
                </div>
            </div>
            <div class="col-xs-4">
                <div class="form-group">
                    {!! Form::select('position_type', [
                    'full-time' => 'Full Time',
                    'part-time' => 'Part Time',
                    'temp'      => 'Temporary',
                    'contract'  => 'Contract',
                    'intern'    => 'Commission'
                    ],isset($jobPosting) ? $jobPosting->jobPositions->first()->blip_name : null, array('class' => 'form-control', 'tabindex' => 3)) !!}
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-6">
        <strong>Contact:</strong>
        <div class="form-group">
            {!! Form::text('contact', null, array('placeholder' => 'Email','class' => 'form-control', 'tabindex' => 4)) !!}
        </div>
    </div>
    <div class="col-lg-6">
        <strong>Location:</strong>
        <div class="row">

            <div class="col-lg-6 col-sm-12">
                <div class="form-group">
                    {!! Form::text('location_city', null, array('placeholder' => 'City','class' => 'form-control', 'tabindex' => 5)) !!}
                </div>
            </div>
            <div class="col-lg-3 col-sm-12">
                <div class="form-group">
                    {!! Form::select('location_state', $states ,null, array('class' => 'form-control', 'tabindex' => 6)) !!}
                </div>
            </div>
            <div class="col-lg-3 col-sm-12">
                <div class="form-group">
                    {!! Form::text('location_zipcode', null, array('placeholder' => 'Zip Code','class' => 'form-control', 'tabindex' => 7)) !!}
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-6 col-sm-12">
        <strong>Compensation: (optional)</strong>
        <div class="row">
            <div class="col-xs-8">
                <div class="form-group">
                    {!! Form::text('salary', null, array('placeholder' => '','class' => 'form-control', 'tabindex' => 8)) !!}
                </div>
            </div>
            <div class="col-xs-4">
                <div class="form-group">
                    {!! Form::select('salary_per', [
                    'hour'  => 'Per Hour',
                    'day'   => 'Per Day',
                    'month' => 'Per Month',
                    'year'  => 'Per Year',
                    ],null, array('class' => 'form-control', 'tabindex' => 9)) !!}
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-6 col-sm-12">
        <div class="form-group">
            <strong>Education Requirement:</strong>
            {!! Form::select('education', [
            'none'        => 'None',
            'high-school' => 'High School',
            'bachelor'    => 'Bachelors',
            'master'      => 'Masters',
            'doctorate'   => 'Doctorate',
            ],null, array('class' => 'form-control', 'tabindex' => 10)) !!}
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
        <div class="form-group">
            <strong>Job Description:</strong>
            {!! Form::textarea('description', null, array('size' => '50x20','id' => 'job-post-textarea','class' => 'form-control')) !!}
        </div>
    </div>
</div>