<table class="table table-sm job-posting-table">
    <thead>
    <tr>
        <th>Company</th>
        <th>Title</th>
        <th class="hidden-sm hidden-xs">Location</th>
        <th class="hidden-md hidden-sm hidden-xs">Created</th>
        <th class="hidden-md hidden-sm hidden-xs">Published</th>
        <th class="hidden-xs">Views</th>
        <th class="hidden-xs">Applications</th>
        <th>Controls</th>
    </tr>
    </thead>
    <tbody>
    @foreach($jobPostings as $jobPosting)
        <tr id="job-posting-{{ $jobPosting->id }}" class="job-posting-row">
            <td>{{ substr($jobPosting->company,0,20) }}</td>
            <td>{{ substr($jobPosting->position,0,20) }}</td>
            <td class="hidden-sm hidden-xs">{{ $jobPosting->location_city . ", " . $jobPosting->location_state }}</td>
            <td class="hidden-md hidden-sm hidden-xs">{{ $jobPosting->created_at }}</td>
            <td class="job-posting-posted">{{ $jobPosting->posted ? 'Yes' : 'No' }}</td>
            <td class="hidden-xs">{{ $jobPosting->view_count ? $jobPosting->view_count : 0 }}</td>
            <td class="hidden-xs">
                @if($jobPosting->application_count)
                    <a href="/job-applications/{{ $jobPosting->id }}">{{ $jobPosting->application_count }}</a>
                @else
                    0
                @endif
            </td>
            <td>
                <a class="btn btn-info" href="{{ route('job.show', $jobPosting->id) }}">Show</a>
                @role('job-poster')
                <a class="btn btn-warning" href="{{ route('job.edit', $jobPosting->id) }}">Edit</a>
                @endif
                @role('super-admin')
                @if($jobPosting->posted)
                    <button class="btn btn-warning job-posting-unpublish" value="{{ $jobPosting->id }}">Unpub</button>
                @else
                    <button class="btn btn-success job-posting-publish" value="{{ $jobPosting->id }}">Pub</button>
                @endif
                @endrole
                <button class="btn btn-primary job-posting-confirm-delete" value="{{ $jobPosting->id }}" data-job-posting-name="{{ $jobPosting->company }} - {{ $jobPosting->position }}">Delete</button>
            </td>
        </tr>
    @endforeach
    </tbody>
    <tfoot>
    <tr>
        <td colspan="8"><a href="/job/create">Add Job Post</a></td>
    </tr>
    </tfoot>
</table>
@if($jobPostings->count() > 0)
    <div class="text-center">
        {{ $jobPostings->appends(isset($input)?$input:'')->links() }}
    </div>
@endif

@section('confirmClickClass','job-posting-delete')
@section('confirmHeader', 'Delete Job Posting')
@section('confirmBody')
    <p>
        Are you sure you want to delete the Job Posting, <strong>"<span id="confirm-job-posting-name"></span></strong>"?
    </p>
@endsection

@include('partials.modal.confirmDelete')