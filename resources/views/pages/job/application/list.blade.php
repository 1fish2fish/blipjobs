@extends('layouts.poster')

@section('title', '')
@section('metaDescription', '')
@section('siteBackground', 'home-background-logo')

@section('poster-content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="row">
                <div class="col-xs-8">
                    <h1>Job Applications</h1>
                </div>
                <div class="col-xs-4">
                    <div class="pull-right">
                        @if(! $applications->lastPage() && $applications->currentPage() != 1)
                            {{ ( $applications->currentPage() * $applications->perPage()) - $applications->perPage() + 1 }} -
                            {{  $applications->currentPage() * $applications->perPage() }} of {{ $applications->total() }}
                        @else
                            {{ ( $applications->currentPage() * $applications->perPage()) - $applications->perPage() + 1 }} -
                            {{  $applications->currentPage() * $applications->perPage() - $applications->perPage() + $applications->count() }}
                            of {{ $applications->total() }}
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="panel-body">

            @include('pages.job.application._table')

        </div>
    </div>
@endsection