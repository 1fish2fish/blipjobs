<table class="table table-sm job-posting-table">
    <thead>
    <tr>
        <th>Name</th>
        <th>Location</th>
        <th class="hidden-sm hidden-xs">Date Applied</th>
        <th class="hidden-md hidden-sm hidden-xs">Cover Letter</th>
        <th class="hidden-md hidden-sm hidden-xs">Resume</th>
    </tr>
    </thead>
    <tbody>
    @foreach($applications as $application)
        @if($application->users)
        <tr>
            <td>
                @if($application->users->first_name | $application->users->last_name )
                {{ $application->users->first_name . " " . $application->users->last_name }}
                @else
                {{ $application->users->email }}
                @endif
            </td>
            <td>
                @if($application->users->userContact)
                {{ $application->users->userContact->city . ", " . $application->users->userContact->state }}
                @endif
            </td>
            <td>{{ $application->created_at->format("M j, Y") }}</td>
            <td>
                @if($application->userCoverLetters)
                    <a href="/files/user/cover-letter/{{ $application->userCoverLetters->id }}" download="cover-letter.txt">Cover Letter</a>
                @endif
            </td>
            <td>
                @if($application->userResumes)
                    <a href="/files/user/resumes/{{$application->userResumes->system_file_name}}" download="{{ $application->userResumes->user_file_name }}">{{ $application->userResumes->user_file_name }}</a>
                @endif
            </td>
        </tr>
        @endif
    @endforeach
    </tbody>
</table>
<div class="text-center">
    {{ $applications->render() }}
</div>
