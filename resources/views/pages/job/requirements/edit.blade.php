@extends('layouts.poster')

@section('title', '')
@section('metaDescription', '')
@section('siteBackground', 'home-background-logo')

@section('poster-content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <h1>Job Posting Edit</h1>
        </div>
        <div class="panel-body">
            {!! Form::open(array('route' => array('job.requirement.update',$jobPosting->id), 'method'=>'PUT')) !!}
            <div class="row">
                <div class="col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2 col-sm-12">
                    @include('pages.job.requirements._form')

                    <div class="row">
                        <div class="col-xs-12">
                            <button type="submit" class="btn btn-primary" tabindex="7">Update</button>
                        </div>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection