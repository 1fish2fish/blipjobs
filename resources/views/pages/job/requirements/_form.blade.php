<div id="req-experiences">
    <div class="row control-label">
        <div class="col-xs-12">
            <strong>Experience Required:</strong>
        </div>
    </div>

    @if($jobPosting && count($jobPosting->jobPostingExperienceReq) > 0)
        @foreach($jobPosting->jobPostingExperienceReq as $expReq)
            <div class="row form-group">
                <div class="col-xs-8">
                    {!! Form::text('experience[' . $expReq->id . '][description]',$expReq->experience, array('placeholder' => 'Experience Description', 'class' => 'form-control')) !!}
                </div>
                <div class="col-xs-3">
                    {!! Form::select('experience[' . $expReq->id . '][years]', [
                    '1' => '1 Year',
                    '2' => '2 Years',
                    '3' => '3 Years',
                    '4' => '4 Years',
                    '5' => '5 Years',
                    '6' => '6 Years',
                    '7' => '7 Years',
                    '8' => '8 Years',
                    '9' => '9 Years',
                    '10' => '10 Years',
                    ],$expReq->years, array('class' => 'form-control')) !!}
                </div>
                <div class="col-xs-1">
                    <button type="button" class="btn btn-primary remove-req-experience"><i class="fa fa-close"></i></button>
                </div>
            </div>
        @endforeach
    @else
        <div class="row form-group">
            <div class="col-xs-8">
                {!! Form::text('experience[0][description]',null, array('placeholder' => 'Experience Description', 'class' => 'form-control')) !!}
            </div>
            <div class="col-xs-3">
                {!! Form::select('experience[0][years]', [
                '1' => '1 Year',
                '2' => '2 Years',
                '3' => '3 Years',
                '4' => '4 Years',
                '5' => '5 Years',
                '6' => '6 Years',
                '7' => '7 Years',
                '8' => '8 Years',
                '9' => '9 Years',
                '10' => '10 Years',
                ],null, array('class' => 'form-control')) !!}
            </div>
        </div>
    @endif

    <div class="row form-group">
        <div class="col-xs-12">
            <button id="add-req-experience" type="button" class="btn btn-info"><i class="fa fa-plus"></i> Experience</button>
        </div>
    </div>
</div>

<div id="req-languages">
    <div class="row control-label">
        <div class="col-xs-12">
            <strong>Language Required:</strong>
        </div>
    </div>

    @if($jobPosting && count($jobPosting->jobPostinglanguageReq) > 0)
        @foreach($jobPosting->jobPostinglanguageReq as $langReq)
            <div class="row form-group">
                <div class="col-xs-8">
                    {!! Form::text('language[]',$langReq->language, array('class' => 'form-control')) !!}
                </div>
                <div class="col-xs-1">
                    <button type="button" class="btn btn-primary remove-req-language"><i class="fa fa-close"></i></button>
                </div>
            </div>
        @endforeach
    @else
        <div class="row form-group">
            <div class="col-xs-8">
                {!! Form::text('language[]',null, array('class' => 'form-control')) !!}
            </div>
        </div>
    @endif
    <div class="row form-group">
        <div class="col-xs-12">
            <button id="add-req-language" type="button" class="btn btn-info"><i class="fa fa-plus"></i> Language</button>
        </div>
    </div>
</div>

<div id="req-licenses">
    <div class="row control-label">
        <div class="col-xs-12">
            <strong>License Required:</strong>
        </div>
    </div>
    @if($jobPosting && count($jobPosting->jobPostinglicenseReq) > 0)
        @foreach($jobPosting->jobPostinglicenseReq as $licReq)
            <div class="row form-group">
                <div class="col-xs-8">
                    {!! Form::text('license[]',$licReq->license, array('class' => 'form-control')) !!}
                </div>
                <div class="col-xs-1">
                    <button type="button" class="btn btn-primary remove-req-license"><i class="fa fa-close"></i></button>
                </div>
            </div>
        @endforeach
    @else
        <div class="row form-group">
            <div class="col-xs-8">
                {!! Form::text('license[]',null, array('class' => 'form-control')) !!}
            </div>
        </div>
    @endif
    <div class="row form-group">
        <div class="col-xs-12">
            <button id="add-req-license" type="button" class="btn btn-info"><i class="fa fa-plus"></i> License</button>
        </div>
    </div>
</div>