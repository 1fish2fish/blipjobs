@extends('layouts.poster')

@section('title', '')
@section('metaDescription', '')
@section('siteBackground', 'home-background-logo')

@section('poster-content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <h1>Job Posting Edit</h1>
        </div>
        <div class="panel-body">

            {!! Form::model($jobPosting, array('route' => ['job.update', $jobPosting->id ],'method'=>'PATCH')) !!}
            @include('pages.job._form')
            <div class="row">
                <div class="col-xs-12 text-center">
                    <button type="submit" class="btn btn-primary" tabindex="7">Update</button>
                    <a href="{{ route('job.requirement.edit',$jobPosting->id) }}"><button type="button" class="btn btn-primary" tabindex="8">Edit Requirements</button></a>
                </div>
            </div>
            {!! Form::close() !!}

        </div>
    </div>
@endsection