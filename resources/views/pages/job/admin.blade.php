@extends('layouts.default')

@section('title', 'Home Page')
@section('metaDescription', 'Blip Jobs is a job search and posting website.  The Site is here to help job seekers to find
employment.  Employers may also use the site to post jobs.  It uses many other sites to aggregate job data.  It also has
functionality to assist users in organizing their job search.')
@section('siteBackground', 'home-background-logo')

@section('content')

<div class="row">
    @include('partials.posterSidebar')
    <div class="col-lg-10 col-sm-9 sidebar-right">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h1>Job Posting Admin Panel</h1>
            </div>
            <div class="panel-body">
                <div class="row job-posting-stat-wrapper">
                    <div class="col-sm-4 col-md-2">
                        <div class="job-posting-stat-tile">
                            <h2>Posts</h2>
                            <span class="job-posting-stat">{{ number_format($stats['job_posts']) }}</span>
                            <div class="job-posting-stat-description">Today</div>
                        </div>
                    </div>
                    <div class="col-sm-4 col-md-2">
                        <div class="job-posting-stat-tile">
                            <h2>Posts</h2>
                            <span class="job-posting-stat">{{ number_format($stats['job_posts_7']) }}</span>
                            <div class="job-posting-stat-description">Last 7 Days</div>
                        </div>
                    </div>
                    <div class="col-sm-4 col-md-2">
                        <div class="job-posting-stat-tile">
                            <h2>Views</h2>
                            <span class="job-posting-stat">{{ number_format($stats['job_views']) }}</span>
                            <div class="job-posting-stat-description">Today</div>
                        </div>
                    </div>
                    <div class="col-sm-4 col-md-2">
                        <div class="job-posting-stat-tile">
                            <h2>Views</h2>
                            <span class="job-posting-stat">{{ number_format($stats['job_views_7']) }}</span>
                            <div class="job-posting-stat-description">Last 7 Days</div>
                        </div>
                    </div>
                    <div class="col-sm-4 col-md-2">
                        <div class="job-posting-stat-tile">
                            <h2>Apps</h2>
                            <span class="job-posting-stat">{{ number_format($stats['job_apps']) }}</span>
                            <div class="job-posting-stat-description">Today</div>
                        </div>
                    </div>
                    <div class="col-sm-4 col-md-2">
                        <div class="job-posting-stat-tile">
                            <h2>Apps</h2>
                            <span class="job-posting-stat">{{ number_format($stats['job_apps_7']) }}</span>
                            <div class="job-posting-stat-description">Last 7 Days</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h1>Job Postings</h1>
            </div>
            <div class="panel-body">
                @include('pages.job._table')
            </div>
        </div>
    </div>
</div>


@endsection