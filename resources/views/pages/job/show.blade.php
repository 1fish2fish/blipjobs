@extends('layouts.poster')

@section('title', '')
@section('metaDescription', '')
@section('siteBackground', 'home-background-logo')

@section('poster-content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <h1>Job Details</h1>
        </div>
        <div class="panel-body">
            <div class="row">
                <div  class="col-md-10 col-md-offset-1">
                    <h1 class="job-posting-company">{{ $jobPosting->company }}</h1>
                    <h2 class="job-posting-position">{{ $jobPosting->position }} - {{ $jobPosting->location_city . ", " . $jobPosting->location_state }}</h2>
                    <div class="job-posting-description">
                        {!! $jobPosting->description !!}
                    </div>

                    @if(count($jobPosting->jobPostingExperienceReq) > 0 )
                        <div class="job-posting-requirement">
                            <h4><strong>Required Experience</strong></h4>
                            <ul>
                                @foreach($jobPosting->jobPostingExperienceReq as $expReq)
                                    <li>{{ $expReq->experience }} - {{ $expReq->years }} yr</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif


                    @if(count($jobPosting->jobPostingLanguageReq) > 0)
                        <div class="job-posting-requirement">
                            <h4><strong>Required Languages</strong></h4>
                            <ul>
                                @foreach($jobPosting->jobPostingLanguageReq as $langReq)
                                    <li>{{ $langReq->language }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif


                    @if(count($jobPosting->jobPostingLicenseReq) > 0)
                        <div class="job-posting-requirement">
                            <h4><strong>Required Licenses</strong></h4>
                            <ul>
                                @foreach($jobPosting->jobPostingLicenseReq as $licReq)
                                    <li>{{ $licReq->license }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <div class="application text-center">
                        <button id="job-application-btn" class="btn btn-primary" data-id="{{ $jobPosting->id }}">Apply</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@include('partials.modal.confirmJobApplication')