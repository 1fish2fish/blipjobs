@extends('layouts.default')

@section('title', 'Home Page')
@section('metaDescription', 'Blip Jobs is a job search and posting website.  The Site is here to help job seekers to find
employment.  Employers may also use the site to post jobs.  It uses many other sites to aggregate job data.  It also has
functionality to assist users in organizing their job search.')
@section('siteBackground', 'home-background-logo')

@section('content')

    <div class="container absolute-center-container">
        <div class="row">
            <div class="col-xs-12">
                <p><strong>Greatest Job Search Engine...</strong></p>
            </div>
        </div>
        <div class="row">
            <form action="search" method="get">
                <div class="col-sm-5">
                    <fieldset class="form-group">
                        <input class="form-control" name="q" placeholder="Job Search" type="text" autofocus tabindex="1">
                        <small class="text-muted">job title, keywords or company</small>
                    </fieldset>
                </div>
                <div class="col-sm-5">
                    <fieldset class="form-group">
                        <input id="location" class="form-control" name="l" placeholder="Location" type="text" tabindex="2">
                        <small class="text-muted">city, state or zip code</small>
                    </fieldset>
                </div>
                <div class="col-sm-2 xs-center">
                    <button class="btn btn-primary" type="submit" tabindex="3">Search</button>
                </div>
            </form>
        </div>
    </div>
@endsection