@extends('layouts.admin')

@section('title', 'User Admin Create')
@section('metaDescription', 'User Admin Create Page')

@section('admin-content')
    <div class="panel">
        <div class="panel-heading">
            <div class="row">
                <div class="col-xs-12">
                    <div class="pull-left">
                        <h1>Create New User</h1>
                    </div>

                    <div class="pull-right">
                        <a class="btn btn-primary" href="{{ route('admin.user.index') }}"> Back</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel-body">
            {!! Form::open(array('route' => 'admin.user.store','method'=>'POST')) !!}
            @include('pages.admin.user._form')
            {!! Form::close() !!}
        </div>
    </div>

@endsection