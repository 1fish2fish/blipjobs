@extends('layouts.admin')

@section('title', 'User Admin Index')
@section('metaDescription', 'User Admin Page')

@section('admin-content')
    <div class="panel">
        <div class="panel-heading">
            <div class="row">
                <div class="col-lg-12 margin-tb">
                    <div class="pull-left">
                        <h1>Users Management</h1>
                    </div>
                    <div class="pull-right">
                        <a class="btn btn-success" href="{{ route('admin.user.create') }}"> Create New User</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel-body">
            <table class="table">
                <tr>
                    <th>No</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Roles</th>
                    <th width="280px">Action</th>
                </tr>
                @foreach ($data as $key => $user)
                    <tr>
                        <td>{{ ++$i }}</td>
                        <td>{{ $user->first_name }} {{ $user->last_name }}</td>
                        <td>{{ $user->email }}</td>
                        <td>
                            @if(!empty($user->roles))
                                @foreach($user->roles as $v)
                                    <label class="label label-success">{{ $v->display_name }}</label>
                                @endforeach
                            @endif
                        </td>
                        <td>
                            <a class="btn btn-info" href="{{ route('admin.user.show',$user->id) }}">Show</a>
                            <a class="btn btn-warning" href="{{ route('admin.user.edit',$user->id) }}">Edit</a>
                            {!! Form::open(['method' => 'DELETE','route' => ['admin.user.destroy', $user->id],'style'=>'display:inline']) !!}
                            {!! Form::submit('Delete', ['class' => 'btn btn-primary']) !!}
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
            </table>
            {!! $data->render() !!}
        </div>
    </div>

@endsection