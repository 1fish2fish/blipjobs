@extends('layouts.admin')

@section('title', 'User Roles Index')
@section('metaDescription', 'User Roles Page')

@section('admin-content')
    <div class="panel">
        <div class="panel-heading">
            <div class="row">
                <div class="col-lg-12 margin-tb">
                    <div class="pull-left">
                        <h1>Role Management</h1>
                    </div>
                    <div class="pull-right">
                        <a class="btn btn-success" href="{{ route('admin.roles.create') }}"> Create New Role</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel-body">
            <table class="table">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Name</th>
                        <th>Description</th>
                        <th width="280px">Action</th>
                    </tr>
                </thead>
                <tbody>
                @foreach ($roles as $key => $role)
                    <tr>
                        <td>{{ ++$i }}</td>
                        <td>{{ $role->display_name }}</td>
                        <td>{{ $role->description }}</td>
                        <td>
                            <a class="btn btn-info" href="{{ route('admin.roles.show',$role->id) }}">Show</a>
                            <a class="btn btn-warning" href="{{ route('admin.roles.edit',$role->id) }}">Edit</a>
                            {!! Form::open(['method' => 'DELETE','route' => ['admin.roles.destroy', $role->id],'style'=>'display:inline']) !!}
                            {!! Form::submit('Delete', ['class' => 'btn btn-primary']) !!}
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            {!! $roles->render() !!}
        </div>
    </div>
@endsection