@extends('layouts.admin')

@section('title', 'User Roles Index')
@section('metaDescription', 'User Roles Page')

@section('admin-content')
    <div class="panel">
        <div class="panel-heading">
            <div class="row">
                <div class="pull-left">
                    <h1>Create New Role</h1>
                </div>
                <div class="pull-right">
                    <a class="btn btn-primary" href="{{ route('admin.roles.index') }}"> Back</a>
                </div>
            </div>
        </div>
        <div class="panel-body">
            {!! Form::open(array('route' => 'roles.store','method'=>'POST')) !!}
            @include('pages.admin.user.roles._form')
            {!! Form::close() !!}
        </div>
    </div>
@endsection