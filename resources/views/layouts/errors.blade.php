<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Blip Jobs - Error @yield('title')</title>
    <meta name="description" content="@yield('metaDescription')">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" type="text/css" href="/css/app.css">
</head>
<body>
<div class="container-fluid">
    @yield('content')
</div>
<script type="text/javascript" src="/js/app.js"></script
</body>
</html>