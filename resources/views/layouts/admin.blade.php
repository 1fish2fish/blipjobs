@extends('layouts.default')

@section('content')
    <div class="row">
        @include('partials.adminSidebar')
        <div class="col-sm-7 col-lg-8">
            @include('partials.messages')
            <div class="admin-content">
                @yield('admin-content')
            </div>
        </div>
    </div>
@endsection