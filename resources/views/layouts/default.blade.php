<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Blip Jobs - @yield('title')</title>
    <meta name="description" content="@yield('metaDescription')">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" type="text/css" href="/css/app.css">
</head>
<body>
    <div id="main-site-container" class="container-fluid @yield('siteBackground', 'default-background')">
        @include('partials.header')
        <div id="main-section">
            @yield('content')
        </div>
        @include('partials.footer')
    </div>
    <meta name="_token" content="{!! csrf_token() !!}" />
    <script type="text/javascript" src="/js/app.js"></script>
    <script type="text/javascript"
            src="http://gdc.indeed.com/ads/apiresults.js"></script>
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-83607066-1', 'auto');
        ga('send', 'pageview');

    </script>
</body>
</html>