@extends('layouts.default')

@section('content')
    <div class="row">
        @include('partials.profileSidebar')
        <div class="col-sm-7 col-lg-8">
            @include('partials.messages')
            @yield('user-content')
        </div>
    </div>
@endsection