@extends('layouts.default')

@section('content')
    <div class="row">
        @include('partials.posterSidebar')
        <div class="col-sm-9 col-lg-8 sidebar-right">
            @include('partials.messages')
            <div class="content">
                @yield('poster-content')
            </div>
        </div>
    </div>
@endsection