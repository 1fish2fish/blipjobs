<div class="col-sm-3 col-lg-2 sidebar">
    <div class="list-group">
        <a href="/admin/user" class="list-group-item{{ strpos(Request::path(),'admin/user') === 0 ? ' active':''}}">Users</a>
        <a href="/admin/roles" class="list-group-item{{ strpos(Request::path(),'admin/roles') === 0 ? ' active':''}}">Roles</a>
    </div>
</div>