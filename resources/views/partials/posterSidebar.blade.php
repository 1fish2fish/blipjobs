<div class="col-sm-3 col-lg-2 sidebar">
    <div class="list-group panel">
        <a href="/" class="list-group-item{{ strpos(Request::path(),'/') === 0 ? ' active':''}}">Job Search</a>
        <a href="/job/admin" class="list-group-item{{ strpos(Request::path(),'job/admin') === 0 ? ' active':''}}">Admin</a>
        <a href="/job" class="list-group-item{{ strpos(Request::path(),'job') === 0  && strpos(Request::path(),'job/admin') !== 0 ? ' active':''}}">Job Posts</a>
        @if(Request::path() == 'job')

        <div class="job-summary-sidebar list-group-item">
            {!! Form::open(array('route' => 'job.index','method'=>'GET')) !!}
            <div class="form-group-sm">
                <label class="control-label">Job Search:</label>
                {!! Form::text('q', isset($input['q']) ? $input['q'] : '', array('placeholder' => '','class' => 'form-control input-sm')) !!}
            </div>
            <div class="form-group-sm">
                <label class="control-label">Location:</label>
                {!! Form::text('l', isset($input['l']) ? $input['l'] : '', array('placeholder' => '','class' => 'form-control input-sm', 'id' => 'location')) !!}
            </div>
            <div class="form-group-sm">
                <label class="control-label">Radius:</label>
                {!! Form::select('r',[
                '0'   => 'Exact Location',
                '5'   => '5 miles',
                '10'  => '10 miles',
                '15'  => '15 miles',
                '25'  => '25 miles',
                '50'  => '50 miles',
                '100' => '100 miles'], isset($input['r']) ? $input['r'] : '5' , array('class' => 'form-control ')) !!}
            </div>
            <div class="form-group-sm">
                <label class="control-label">Job Type:</label>
                {!! Form::select('jt',[
                null         => 'All',
                'temporary'  => 'Temporary',
                'parttime'   => 'Part Time',
                'fulltime'   => 'Full Time',
                'internship' => 'Internship',
                'contract'   => 'Contract',
                'commission' => 'Commission'], isset($input['jt']) ? $input['jt'] : null, array('class' => 'form-control ')) !!}
            </div>
            <div class="form-group-sm">
                <label class="control-label">Posting Age:</label>
                {!! Form::select('fromage',[
                'any'    => 'Anytime',
                '15'     => 'within 15 days',
                '7'      => 'within 7 days',
                '3'      => 'within 3 days',
                '1'      => 'since yesterday'], isset($input['fromage']) ? $input['fromage'] : 'any', array('class' => 'form-control ')) !!}
            </div>
            <div class="form-group-sm">
                <label class="control-label">Published:</label>
                {!! Form::select('published', [
                    'all' => 'All',
                    'yes'  => 'Yes',
                    'no'   => 'No',
                    ],isset($input['published']) ? $input['published']: 'all', array('class' => 'form-control', 'tabindex' => 9)) !!}

            </div>
            @role('super-admin')
            <div class="form-group-sm">
                <label class="control-label">Source:</label>
                {!! Form::select('source', \App\Models\JobSource::getSourceSelect(),isset($input['source']) ? $input['source']: 0, array('class' => 'form-control', 'tabindex' => 10)) !!}

            </div>
            @endrole
            <div class="form-group-sm">
                <label class="control-label">Order By:</label>
                {!! Form::select('order_by', [
                    'company'    => 'Company',
                    'position'   => 'Title',
                    'created_at' => 'Created Date',
                    ],isset($input['order_by']) ? $input['order_by']: 'created_at', array('class' => 'form-control', 'tabindex' => 11)) !!}

            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 text-center submit">
                    <button type="submit" class="btn btn-primary">Filter</button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
        @endif
    </div>
</div>