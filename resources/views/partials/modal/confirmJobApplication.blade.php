<div class="modal fade" id="confirm-job-application-modal" role="dialog"  data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                Apply to {{ $jobPosting->company }} for the position {{ $jobPosting->position }}
            </div>
            <div class="modal-body">
                <div class="alert alert-dismissable alert-danger hidden">
                    <strong>Error</strong><br/>
                    <div id="confirm-job-application-modal-error">

                    </div>
                </div>

                @if(Auth::user())
                    <div class="row">
                        <div class="col-xs-12 form-group-sm margin-bottom">
                            <label>Cover Letter</label>
                            {{ Form::select('cover-letter',Auth::user()->getCoverLetterArray(), Auth::user()->getDefaultCoverLetter(), ['class' => 'form-control']) }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 form-group-sm">
                            <label>Resume</label>
                            {{ Form::select('resume',Auth::user()->getResumesArray(), Auth::user()->getDefaultResume(), ['class' => 'form-control']) }}
                        </div>
                    </div>

                @else
                    <p>You must be logged in to apply to jobs.  Login or Register.</p>

                    <a href="/login">Login</a><br/>
                    <a href="/register">Register</a>
                @endif
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" tabindex="1" autofocus>Cancel</button>
                @if(Auth::user())
                <button id="job-application-apply" class="btn btn-danger btn-ok" value=""
                        data-job-posting-id="{{ $jobPosting->id }}"
                        data-loading-text="Applying..." tabindex="2">Apply</button>
                @endif
            </div>
        </div>
    </div>
</div>