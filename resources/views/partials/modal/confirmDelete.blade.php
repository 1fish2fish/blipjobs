

<div class="modal fade" id="confirm-delete" role="dialog"  data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                @yield('confirmHeader')
            </div>
            <div class="modal-body">
                @yield('confirmBody')
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" tabindex="1" autofocus>Cancel</button>
                <button class="btn btn-danger btn-ok @yield('confirmClickClass')" value="" data-loading-text="Deleting..." tabindex="2">Delete</button>
            </div>
        </div>
    </div>
</div>