<div class="col-sm-3 col-lg-2 sidebar">
    <div class="list-group">
        <a href="/" class="list-group-item">Job Search</a>
        @role('job-poster')
        <a href="/job/admin" class="list-group-item{{ strpos(Request::path(),'job/admin') === 0 ? ' active':''}}">Admin</a>
        @endrole
        <a href="/user/profile" class="list-group-item{{ strcmp(Request::path(),'user/profile') === 0 ? ' active':''}}">User</a>
        <a href="/user/resumes" class="list-group-item{{ strpos(Request::path(),'user/resumes') === 0 ? ' active':''}}">Resumes</a>
        <a href="/user/cover-letters" class="list-group-item{{ strpos(Request::path(),'user/cover-letters') === 0 ? ' active':''}}">Cover Letters</a>
    </div>
</div>
