

<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" onclick="$('#main-section').toggle(600)" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-user-menu" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <div class="logo">
                <a href="/"><img src="/img/blip-font-only.png" alt="Blip Jobs Home"></a>
            </div>
        </div>


        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="main-user-menu">

            <ul class="nav navbar-nav navbar-right">
                @if(Auth::check())
                    <li>
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{ Auth::user()->email}} <span class="caret"></span></a>
                        <ul class="dropdown-menu transparent-panel">
                            <li><a href="/user/profile">Profile</a></li>
                    @role(['job-poster','super-admin'])
                            <li><a href="/job/admin">Admin</a></li>
                    @endrole
                    @role('super-admin')
                            <li><a href="/admin/user">User Admin</a></li>
                    @endrole

                        <li><a href="/logout">Log Out</a></li>
                        </ul>
                    </li>
                @else
                    <li><a href="/login">Log In</a></li>
                    <li><a href="/register">Register</a></li>
                @endif
            </ul>
        </div><!-- /.navbar-collapse -->
    </div>
</nav>