require('jquery');
var autocomplete = require('jquery-autocomplete');

var languages = [
    "English",
    "Spanish",
    "Portugese"
];

$(document).ready(function() {

    $( "#language" ).autocomplete({
        source: languages
    });

});