window.$ = window.jQuery = require('jquery');
require('jquery-ui-dist/jquery-ui.min.js');
require('bootstrap-sass');
require('./user/resume.js');
require('./user/cover-letter.js');
require('./admin/roles.js');
require('./register.js');
require('./jobs.js');
require('./post/job.js');
require('./data/city_state.js');

/*
    Tile box shadow when mouse in.
 */
$(".tile").bind("mouseenter mouseleave", function() {
    $(this).toggleClass("tile-active");
})

/*
  File Input with bootstrap button.

  This allows the hidden input value to be added to the next input

  Reference: https://www.abeautifulsite.net/whipping-file-inputs-into-shape-with-bootstrap-3

 */
$(function() {

    // We can attach the `fileselect` event to all file inputs on the page
    $(document).on('change', ':file', function() {
        var input = $(this),
            numFiles = input.get(0).files ? input.get(0).files.length : 1,
            label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
        input.trigger('fileselect', [numFiles, label]);
    });

    // We can watch for our custom `fileselect` event like this
    $(document).ready( function() {
        $(':file').on('fileselect', function(event, numFiles, label) {

            var input = $(this).parents('.input-group').find(':text'),
                log = numFiles > 1 ? numFiles + ' files selected' : label;

            if( input.length ) {
                input.val(log);
            } else {
                if( log ) alert(log);
            }

        });
    });

});

$.ajaxSetup({

    headers: {

        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')

    }

});

$(".btn-toggle").click(function(e){
    e.preventDefault();
    $(this).find('.btn-info').removeClass("btn-info");
    $(e.target).addClass("btn-info");
    var data_input_id = $(this).data('input-id');
    $("#" + data_input_id).val($(e.target).data('value'));
});