$(".register-role").click(function (e) {
    e.preventDefault();

    $("#register-role-input").val($(this).data('value'));
    $(".register-form-header").html("Register " + $(this).html());
});