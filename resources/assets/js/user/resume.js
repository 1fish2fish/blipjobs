$(".resume-delete").click(function() {
    var id = $(this).val();
    $(this).button('loading');

    $.ajax ({
        url: "/user/resumes/" + id,
        type: 'DELETE',
        success: function (result) {
            $("#resume-" + id).remove();
            $("#confirm-delete").modal("hide");
        },
        error: function (result) {

        }
    });
});

$(".resume-update-default").click(function(e) {
    e.preventDefault();
    var id = $(this).data("id");
    var data = {'_method' : 'patch'};
    $('.loading-overlay').show();

    $.ajax ({
        url  : "/user/resumes/" + id,
        type : 'POST',
        data : data,
        success: function (result) {
            $(".resume .active-default").addClass('default');
            $(".resume .active-default").removeClass('active-default');
            $("#resume-" + id + " .resume-update-default").removeClass('default').addClass('active-default');
            $('.loading-overlay').hide();
        },
        error: function (result) {

        }
    });
})

$(".resume-confirm-delete").click(function() {
    var id = $(this).val();
    $(".resume-delete").val(id);
    $("#confirm-resume-name").html($(this).data("resumeName"));
    $("#confirm-delete").modal("show");
})