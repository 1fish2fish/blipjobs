$(".cover-letter-delete").click(function() {
    var id = $(this).val();
    $(this).button('loading');

    $.ajax ({
        url: "/ajax/user/cover-letters/" + id,
        type: 'DELETE',
        success: function (result) {
            $("#cover-letter-" + id).remove();
            $("#confirm-delete").modal("hide");
        },
        error: function (result) {

        }
    });
});

$(".cover-letter-update-default").click(function(e) {
    e.preventDefault();
    var id = $(this).data("id");
    var data = {'_method' : 'patch'};
    $('.loading-overlay').show();

    $.ajax ({
        url  : "/ajax/user/cover-letters/" + id,
        type : 'PUT',
        data : data,
        success: function (result) {
            $(".cover-letter .active-default").addClass('default');
            $(".cover-letter .active-default").removeClass('active-default');
            $("#cover-letter-" + id + " .cover-letter-update-default").removeClass('default').addClass('active-default');
            $('.loading-overlay').hide();
        },
        error: function (result) {

        }
    });
})

$(".cover-letter-confirm-delete").click(function() {
    var id = $(this).val();
    $(".cover-letter-delete").val(id);
    $("#confirm-job-posting-name").html($(this).data("job-posting-name"));
    $("#confirm-delete").modal("show");
})