var job_experience = 1;

$('#add-req-experience').on('click',this,function() {

    var row = '<div class="row">' +
                '<div class="col-xs-8">' +
                    '<div class="form-group">' +
                        '<input placeholder="Experience Description" class="form-control" name="experience[' + job_experience + '][description]" type="text">' +
                    '</div>' +
                '</div>' +
                '<div class="col-xs-3">' +
                    '<div class="form-group">' +
                        '<select class="form-control" name="experience[' + job_experience + '][years]">' +
                            '<option value="1">1 Year</option>' +
                            '<option value="2">2 Years</option>' +
                            '<option value="3">3 Years</option>' +
                            '<option value="4">4 Years</option>' +
                            '<option value="5">5 Years</option>' +
                            '<option value="6">6 Years</option>' +
                            '<option value="7">7 Years</option>' +
                            '<option value="8">8 Years</option>' +
                            '<option value="9">9 Years</option>' +
                            '<option value="10">10 Years</option>' +
                        '</select>' +
                    '</div>' +
                '</div>' +
                '<div class="col-xs-1">' +
                    '<button type="button" class="btn btn-primary remove-req-experience"><i class="fa fa-close"></i></button>' +
                '</div>' +
              '</div>';

    job_experience++;

    $(this).before(row);
});

$('#req-experiences').on('click','.remove-req-experience', function () {
    $(this).closest('.row').remove();
});

$('#add-req-language').on('click',this,function() {

    var row =   '<div class="row">' +
                    '<div class="col-xs-8">' +
                        '<div class="form-group">' +
                            '<input placeholder="" class="form-control" name="language[]" type="text">' +
                        '</div>' +
                    '</div>' +
                    '<div class="col-xs-1">' +
                        '<button type="button" class="btn btn-primary remove-req-language"><i class="fa fa-close"></i></button>' +
                    '</div>' +
                '</div>';

    $(this).before(row);
});

$('#req-languages').on('click','.remove-req-language', function () {
    $(this).closest('.row').remove();
});


$('#add-req-license').on('click',this,function() {

    var row =   '<div class="row">' +
        '<div class="col-xs-8">' +
        '<div class="form-group">' +
        '<input placeholder="" class="form-control" name="license[]" type="text">' +
        '</div>' +
        '</div>' +
        '<div class="col-xs-1">' +
        '<button type="button" class="btn btn-primary remove-req-license"><i class="fa fa-close"></i></button>' +
        '</div>' +
        '</div>';

    $(this).before(row);
});

$('#req-licenses').on('click','.remove-req-license', function () {
    $(this).closest('.row').remove();
});

$(".job-posting-delete").click(function() {
    var id = $(this).val();
    $(this).button('loading');

    $.ajax ({
        url: "/post/job/" + id,
        type: 'DELETE',
        success: function (result) {
            $("#job-posting-" + id).remove();
            $("#confirm-delete").modal("hide");
            $(".job-posting-delete").button('reset');
        },
        error: function (result) {

        }
    });
});

$(".job-posting-confirm-delete").click(function() {
    var id = $(this).val();
    $(".job-posting-delete").val(id);
    $("#confirm-job-posting-name").html($(this).data("job-posting-name"));
    $("#confirm-delete").modal("show");
})