$(".job-link").click(function (e) {

    var button_style = {
        'viewed'         : 'btn-info',
        'applied'        : 'btn-success',
        'interview'      : 'btn-success',
        'offer'          : 'btn-success',
        'not-interested' : 'btn-warning'
    };

    var button_text = {
        'viewed'         : 'Viewed',
        'applied'        : 'Applied',
        'interview'      : 'Interview',
        'offer'          : 'Offer',
        'not-interested' : 'No Interest'
    };

    var data = {
        'status' : $(this).data('status'),
        'job_id' : $(this).data('id')

    };

    var job_link_button = $("#status-" + data.job_id + " .job-status-current");
    var send_ajax       = true;

    if(job_link_button.html() != "New" && data.status == "viewed") {
        send_ajax = false;
    }

    if(send_ajax) {
        $.ajax ({
            url: "/ajax/user/job-status",
            type: 'POST',
            data: data,
            success: function (result) {
                job_link_button.html(button_text[data.status]);
                job_link_button.parent().removeClass();
                job_link_button.parent().addClass("job-status-btn btn btn-sm dropdown-toggle " + button_style[data.status]);
            },
            error: function (result) {

            }
        });
    }
});

$('.job-posting-table').on('click','.job-posting-publish',function (e) {

    var button = $(this);
    $.ajax ({
        url: "/admin/ajax/job-publish",
        type: 'POST',
        data: {'id' : $(this).val()},
        success: function (result) {
            button.removeClass('job-posting-publish');
            button.removeClass('btn-success');
            button.addClass('btn-warning');
            button.addClass('job-posting-unpublish');
            button.html('Unpub');
            button.closest('.job-posting-row').find('.job-posting-posted').html('Yes');
        },
        error: function (result) {



        }
    });
});

$('.job-posting-table').on('click','.job-posting-unpublish', function (e) {

    var button = $(this);
    $.ajax ({
        url: "/admin/ajax/job-unpublish",
        type: 'POST',
        data: {'id' : $(this).val()},
        success: function (result) {
            button.removeClass('btn-warning');
            button.removeClass('job-posting-unpublish');
            button.addClass('job-posting-publish');
            button.addClass('btn-success');
            button.html('Pub');
            button.closest('.job-posting-row').find('.job-posting-posted').html('No');
        },
        error: function (result) {



        }
    });
});

$('#job-application-btn').click(function (e) {
    $('#confirm-job-application-modal').modal("show");
});

$('#job-application-apply').click(function (e) {

    var data = {
        'job_posting_id' : $(this).data('job-posting-id'),
        'user_resume_id' : $('select[name=resume]').val(),
    };

    if($("select[name=cover-letter]").val() != 0) {
        data['user_cover_letter_id'] = $("select[name=cover-letter]").val();
    }

    $.ajax ({
        url: "/ajax/job/apply",
        type: 'POST',
        data: data,
        success: function (result) {
            $('#confirm-job-application-modal').modal("hide");
            $('#job-application-btn').prop('disabled',true).html('Applied');
        },
        error: function (result) {
            $('#confirm-job-application-modal-error').html(result.responseJSON.error);
            $('.alert').removeClass('hidden');
        }

    });


});