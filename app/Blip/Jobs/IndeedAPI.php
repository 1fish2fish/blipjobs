<?php namespace App\Blip\Jobs;

use App\Models\JobPosting;
use App\Models\JobSource;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use JobBrander\Jobs\Client\Providers\Indeed;
use Goutte\Client as ScrapeClient;

class IndeedAPI extends Indeed
{
    private $resultCount;
    private $start;
    private $end;
    private $position;
    private $user_id;

    public function __construct($parameters = [])
    {
        $this->includeLatLong(1);
        $this->queryParams['salary'] = '';
        $this->source                = JobSource::firstOrCreate(["name" => "IndeedAPI"]);

        parent::__construct($parameters);
    }

    public function createJobObject($payload)
    {
        $payload = static::parseAttributeDefaults($payload, $this->jobDefaults);
        $description = '';

        if (isset($payload['url'])) {
            $description = $this->scrapeDescription($payload['url']);
        }

        $job = new JobPosting();

        $job->company        = isset($payload['company']) ? ucwords(strtolower($payload['company'])) : '';
        $job->position       = isset($payload['jobtitle']) ? ucwords(strtolower($payload['jobtitle'])) : '';
        $job->description    = $description?$description:$payload['snippet'];
        $job->posted_at      = isset($payload['date']) ? new Carbon($payload['date']) : new Carbon();
        $job->created_at     = new Carbon();
        $job->posted         = 1;
        $job->url            = isset($payload['url']) ? $payload['url'] : '';
        $job->job_source_id  = $this->source->id;

        if(isset($this->position)) {
            $job->location_city   = ucwords(strtolower($this->position->city));
            $job->location_state  = $this->position->state;
            $job->location_zip    = $this->position->zipcode;
            $job->location_latlon = $this->position->latlon;
        } else {
            $location             = $this->getJobLocation($payload['formattedLocationFull']);
            $job->location_city   = ucwords(strtolower($location['city']));
            $job->location_state  = $location['state'];
            $job->location_zip    = $location['zip'];
            $job->location_latlon = $payload['latitude'] . ',' . $payload['longitude'];
        }

        $job->user_id        = $this->user_id;
        $job->setJobSourceKey();

        return $job;
    }

    public function getEnd()
    {
        return $this->end;
    }

    public function getJobs()
    {
        return parent::getJobs();
    }

    public function getNext()
    {
        if (isset($this->start) && $this->end < $this->resultCount) {
            return $this->end;
        } else {
            return 0;
        }
    }

    public function getPrevious()
    {
        if ($this->start > 1) {
            return $this->start;
        } else {
            return false;
        }
    }

    public function getResultCount()
    {
        return $this->resultCount;
    }

    public function getStart()
    {
        return $this->start;
    }

    public function scrapeDescription($url)
    {
        $scraper = new ScrapeClient();

        $crawler = $scraper->request('GET', $url);

        if($description = $crawler->filter('#job_summary')) {
            return $description->text();
        } else {
            return false;
        }
    }

    public function setPosition($position)
    {
        $this->position = $position;
    }

    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
    }

    protected function getRawListings(array $payload = array())
    {
        $this->saveResults($payload);

        return parent::getRawListings($payload);
    }

    protected function getJobsCollectionFromListings(array $listings = array())
    {
        $collection = new Collection();

        array_map(function ($item) use ($collection) {
            $job = $this->createJobObject($item);
            $collection->add($job);
        }, $listings);

        return $collection;
    }

    private function getJobLocation($location)
    {
        $parts = explode(",",$location);
        $city  = '';
        $state = '';
        $zip   = '';

        if (isset($parts[0])) {
            $city = $parts[0];
        }
        if (isset($parts[1])) {
            $stateZip = explode(" ", trim($parts[1]));
            if(isset($stateZip[0])) {
                $state = $stateZip[0];
            }

            if(isset($stateZip[1])) {
                $zip = $stateZip[1];
            }
        }

        return ['city' => $city, 'state' => $state, 'zip' => $zip];
    }

    private function saveResults($payload)
    {
        $this->resultCount = isset($payload['totalResults']) ? $payload['totalResults'] : 0;
        $this->start = isset($payload['start']) ? $payload['start'] : 0;
        $this->end = isset($payload['end']) ? $payload['end'] : 0;
    }
}