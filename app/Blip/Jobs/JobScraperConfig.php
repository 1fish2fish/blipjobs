<?php
namespace App\Blip\Jobs;

class JobScraperConfig
{
    public $company;

    //HTML Parts to find information.
    public $postingsPart;
    public $positionPart;
    public $companyPart;
    public $locationPart;
    public $postedDatePart;
    public $descriptionPart;
    public $nextPage;
    public $modSearch;

    public $url;

}