<?php

namespace App\Blip\Jobs;

class CareerJetAPI {
    var $locale = '' ;
    var $version = '3.6';
    var $careerjet_api_content = '';


    public function __construct( $locale = 'en_US' )
    {
        $this->locale = $locale;
    }

    public function call($fname , $args)
    {
        $url = 'http://public.api.careerjet.net/'.$fname.'?locale_code='.$this->locale;

        if (empty($args['affid'])) {
            return (object) array(
                'type' => 'ERROR',
                'error' => "Your Careerjet affiliate ID needs to be supplied. If you don't " .
                    "have one, open a free Careerjet partner account."
            );
        }

        foreach ($args as $key => $value) {
            $url .= '&'. $key . '='. urlencode($value);
        }

        $url .= '&user_ip=' . '192.168.0.1';
        $url .= '&user_agent=' . urlencode('Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36');

        $header = "User-Agent: careerjet-api-client-v" . $this->version . "-php-v" . phpversion();

        $careerjet_api_context = stream_context_create(array(
            'http' => array('header' => $header)
        ));

        $response = file_get_contents($url, false, $careerjet_api_context);
        return json_decode($response);
    }

    public function search($args)
    {
        $result =  $this->call('search' , $args);
        if ($result->type == 'ERROR') {
            echo "API Error (careerJetAPI)\n";
            echo var_dump($result);

            return false;
        }
        return $result;
    }
}