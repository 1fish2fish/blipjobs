<?php
namespace App\Blip\Jobs;

use App\Blip\Geocode;
use App\Models\JobPosting;
use App\Models\JobSource;
use App\Models\User;
use Behat\Mink\Driver\Selenium2Driver;
use Behat\Mink\Session;
use Carbon\Carbon;
use Illuminate\Support\Collection;

class JobScraper
{

    protected $configs;
    protected $browser;

    public function __construct($configs)
    {
        $this->source  = JobSource::firstOrCreate(["name" => "Job Scraper"]);
        $this->user    = User::firstOrCreate(['email' => 'drew.ostrowski@gmail.com']);
        $driver        = new Selenium2Driver();
        $this->browser = new Session($driver);
        $this->browser->start();

        $this->configs = $configs;
    }

    public function setURL($url)
    {
        $this->browser->visit($url);
    }

    public function scrapePostingsFromPage()
    {
        $postings     = new Collection();
        $page         = $this->browser->getPage();
        $this->browser->wait(10000, "document.querySelector('" . addslashes($this->configs->postingsPart) . "')");
        $postingLinks = $page->findAll('css',$this->configs->postingsPart);

        foreach($postingLinks as $postingLink) {
            $this->browser->wait(10000, "document.querySelector('" . addslashes($this->configs->postingsPart) . "')");

            try {
                if($postingLink->hasAttribute('href')) {
                    $uri = $postingLink->getAttribute('href');
                    $url = $this->createURL($uri);
                    $postingLink->click();
                    $this->browser->wait(10000, "document.querySelector('" . addslashes($this->configs->locationPart) . "')");
                    $postings = $postings->merge($this->getPosting($url));
                    $this->browserBack();
                    $this->browser->wait(10000, "document.querySelector('" . addslashes($this->configs->postingsPart) . "')");
                }
            } catch(\Exception $e) {
                echo $e->getMessage();
            }
        }

        return $postings;
    }

    public function createURL($uri)
    {
        if (strpos($uri,"https") === 0) {
            return $uri;
        } else if (strpos($uri,"http") === 0) {
            return $uri;
        } else if (strpos($uri,"/") === 0) {
            $base_url = substr($this->configs->url, 0, strpos($this->configs->url, "/"));
            return $base_url . $uri;
        } else {
            return $this->configs->url . $uri;
        }
    }

    public function browserBack()
    {
        if($this->configs->back) {
            $page = $this->browser->getPage();
            if($page->has('css',$this->configs->back)) {
                $back = $page->find('css',$this->configs->back);
                $back->click();
            } else {
                throw new Exception("can't find back button");
            }
        } else {
            $this->browser->back();
        }
    }

    public function modifySearch() {

        $page = $this->browser->getPage();

        foreach($this->configs->modSearch as $modStep) {

            if($modStep['type'] == 'link') {
                $modificationControls = $page->findLink($modStep['search']);
            } else {
                $modificationControls = $page->findAll($modStep['type'],$modStep['search']);
            }

            foreach($modificationControls as $modControl) {
                $modControl->click();
            }
        }
    }

    public function getPosting($url)
    {
        $jobPostings = [];

        $page    = $this->browser->getPage();
        $locations   = $this->getLocations($page);
        foreach($locations as $location) {

            if($location->city && $location->state) {

                $jobPosting = new JobPosting();

                $jobPosting->company  = $this->getCompany($page);
                $jobPosting->position = $this->getPosition($page);
                $jobPosting->description = $this->getDescription($page);
                $jobPosting->posted_at   = new Carbon($this->getPostedDate($page));
                $jobPosting->created_at  = new Carbon();
                $jobPosting->location_latlon      = $location->latlon;
                $jobPosting->location_city        = $location->city;
                $jobPosting->location_state       = $location->state;
                $jobPosting->location_zip         = $location->zipcode?$location->zipcode:0;
                $jobPosting->job_source_id        = $this->source->id;
                $jobPosting->user_id              = $this->user->id;
                $jobPosting->posted               = 1;
                $jobPosting->url                  = $url;
                $jobPosting->setJobSourceKey();

                $jobPostings[$jobPosting->job_source_key] = $jobPosting;
            }
        }

        return collect($jobPostings);
    }

    public function getNextPage()
    {
        $page = $this->browser->getPage();

        $next = $page->find("css",$this->configs->nextPage);

        if($next) {
            $next->click();
            return true;
        }

        return false;
    }

    public function getPosition($page)
    {
        if(!$this->configs->positionPart) {
            throw new Exception('config Position Part Needed.');
        }

        if($position = $page->find("css",$this->configs->positionPart)) {
            return $position->getText();
        }
    }

    public function getCompany($page)
    {
        if(!$this->configs->company && !$this->configs->companyPart) {
            throw new Exception('config Company Part Needed.');
        }

        if($this->configs->company) {
            return $this->configs->company;
        } else {
            if($company = $page->find("css",$this->configs->companyPart)) {
                return $company->getText();
            }
        }
    }

    public function getLocations($page)
    {
        $geoLocations = [];

        if(!$this->configs->locationPart) {
            throw new Exception('config Location Part Needed.');
        }

        $locations = $page->findAll("css",$this->configs->locationPart);

        foreach($locations as $location) {
            $positions = Geocode::getPosition($location->getText());
            if(count($positions) > 0) {
                $geoLocations[] = $positions[0];
            }
        }

        return $geoLocations;
    }

    public function getPostedDate($page)
    {
        if(!$this->configs->postedDatePart) {
            throw new Exception('config Posted Part Needed.');
        }

        if($date = $page->find("css",$this->configs->postedDatePart)) {
            return $date->getText();
        }
    }

    public function getDescription($page)
    {
        $description = "";

        if(!$this->configs->descriptionPart) {
            throw new Exception('config Description Part Needed.');
        }

        foreach($this->configs->descriptionPart as $part) {
            if($page->has("css",$part)) {
                $description .= $page->find("css",$part)->getText();
            }
        }

        return $description;
    }
}