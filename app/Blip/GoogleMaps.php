<?php
/**
 * Created by PhpStorm.
 * User: Drew Ostrowski
 * Date: 8/16/2016
 * Time: 4:01 PM
 */

namespace App\Blip;


use App\Models\PositionZipcode;
use GuzzleHttp\Client;

class GoogleMaps
{
    public static $addressConponenTypes = [
        'administrative_area_level_1' => 'state',
        'administrative_area_level_2' => 'county',
        'locality'                    => 'city',
        'postal_code'                 => 'zipcode',
        'country'                     => 'country',
    ];

    protected $apiKey;

    protected $partial_match = true;

    public function __construct($apiKey)
    {
        $this->httpClient = new Client(['base_uri' => 'https://maps.googleapis.com/maps/api/']);
        $this->apiKey = $apiKey;
    }

    public function geocode($address)
    {
        $positions = [];

        $res        = $this->httpClient->request('GET', "geocode/json?address=$address&key=" . $this->apiKey);

        $rawResults = json_decode($res->getBody()->getContents());


        foreach($rawResults->results as $raw) {

            if(!property_exists($raw,'partial_match') || $raw->partial_match == $this->partial_match) {
                $location = new Location();

                foreach($raw->address_components as $add_part) {
                    if(array_key_exists($add_part->types[0],self::$addressConponenTypes)) {
                        $data_member = self::$addressConponenTypes[ $add_part->types[0]];
                        $location->{$data_member} = $add_part->short_name;
                    }
                }

                $location->latlon = $raw->geometry->location->lat . "," . $raw->geometry->location->lng;

                $positions[] = $location;
            }
        }

        return $positions;
    }


}