<?php

namespace App\Blip;

class Location
{
    public $city;
    public $state;
    public $zipcode;
    public $county;
    public $country;
    public $latlon;


    public function hasData()
    {
        if(isset($this->city) & isset($this->state)) {
            return true;
        } else if(isset($this->zipcode)) {
            return true;
        } else if(isset($this->county) && isset($this->state)) {
            return true;
        } else {
            return false;
        }
    }

    public function getLat()
    {
        $latlon = explode(",",$this->latlon);

        return $latlon[0];
    }

    public function getLon()
    {
        $latlon = explode(",",$this->latlon);

        return $latlon[1];
    }


}