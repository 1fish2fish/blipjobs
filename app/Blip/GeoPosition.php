<?php
/*
 * To be used with models to add geo location capabilities.
 */


namespace App\Blip;

use Illuminate\Support\Facades\DB;

trait GeoPosition {

    private $location;

    public function __set($key, $value)
    {
        if(in_array($key, $this->geofields)) {
            $this->attributes[$key] = DB::raw("POINT($value)");

            return $this;
        } else {

            return parent::__set($key, $value);
        }
    }

    public function __get ($key)
    {
        if(in_array($key, $this->geofields)) {
            $loc = substr($this->attributes[$key], 6);
            $loc = preg_replace('/[ ,]+/', ',', $loc, 1);

            return substr($loc, 0, -1);
        } else {

            return parent::__get($key);
        }
    }

    public function getLat()
    {
        return explode(",",$this->latlon)[0];
    }

    public function getLon()
    {
        return explode(",",$this->latlon)[1];
    }

    public function newQuery($excludeDeleted = true)
    {
        $raw='';
        foreach($this->geofields as $column){
            $raw .= ' astext('.$column.') as '.$column.' ';
        }

        return parent::newQuery($excludeDeleted)->addSelect('*',DB::raw($raw));
    }


    //distance in miles  1 degree = 69 miles approx.
    public function scopeDistance($query,$dist,$location)
    {
        return $query->whereRaw('st_distance(' . $this->geofields[0] . ',POINT('.$location.')) < '.$dist/69);

    }

}