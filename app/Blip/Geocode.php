<?php

namespace App\Blip;

use App\Models\PositionCity;
use App\Models\PositionCounty;
use App\Models\PositionZipcode;

class Geocode {

    public static function getPosition($locationString)
    {
        $location = self::parseLocationString($locationString);

        if($location->hasData()) {
            return [$location];
        } else {
            $locations = self::googleGeocode($locationString);
            $locations = self::usOnly($locations);

            self::updateDB($locations);

            return $locations;
        }

    }

    public static function usOnly($locations)
    {
        $usLocations = [];

        foreach($locations as $location) {
            if($location->country == 'US') {
                $usLocations[] = $location;
            }
        }

        return $usLocations;
    }


    public static function googleGeocode($locationString)
    {
        $googleApi = new GoogleMaps(env('GOOGLE_MAPS_API'));

        return $googleApi->geocode($locationString);
    }

    /*
     * Parses A String and separates out city, state, zip,
     * and county. Returns a Location Object with
     * Information separated. false if not.
     */
    public static function parseLocationString($locationString)
    {
        $location = new Location();

        if (preg_match("/^\d{5}$/",$locationString,$matches)) { // zipcode - 99999

            if($pos = PositionZipcode::where('zipcode',$matches[0])->first()) {
                $location->zipcode = $matches[0];
                $location->latlon  = $pos->latlon;
            }

        } elseif(UsStates::isState($locationString)) { // state

            $location->state = UsStates::getAbbreviation($locationString);

        } elseif (preg_match("/^((\w+\W?)+),\W?(\w{2})$/",$locationString,$matches)) { // city or county , state - town, ca

            if($pos = PositionCity::where([
                ['city',strtoupper($matches[1])],
                ['state',(strtoupper($matches[3]))]])->first()) {

                $location->city   = $matches[1];
                $location->state  = $matches[3];
                $location->latlon = $pos->latlon;
            }
            if($pos = PositionCounty::where([
                ['county',strtoupper($matches[1])],
                ['state',(strtoupper($matches[3]))]])->first()) {
                $location->county = $matches[1];
                $location->state  = $matches[3];
                $location->latlon = $pos->latlon;
            }
        } elseif (preg_match("/^((\w+\W?)+),\W?(\w{2}),(\W?(\w+\W?)+)$/",$locationString,$matches)) { // city or county , state, Country - town, ca, USA

            if(strtoupper($matches[5]) == 'USA' || strtoupper($matches[5]) == 'USA') {
                if($pos = PositionCity::where([
                    ['city',strtoupper($matches[1])],
                    ['state',(strtoupper($matches[3]))]])->first()) {

                    $location->city   = $matches[1];
                    $location->state  = $matches[3];
                    $location->latlon = $pos->latlon;
                }

                if($pos = PositionCounty::where([
                    ['county',strtoupper($matches[1])],
                    ['state',(strtoupper($matches[3]))]])->first()) {
                    $location->county = $matches[1];
                    $location->state  = $matches[3];
                    $location->latlon = $pos->latlon;
                }
            }
        }

        return $location;
    }

    public static function updateDB($locations)
    {
        foreach($locations as $location) {

            if($location->country == 'US') {
                if($location->city && $location->state && $location->zipcode && $location->latlon ) { //insert zipcode and or city.
                    if (!PositionZipcode::where('zipcode', $location->zipcode)->exists()) { //insert zip
                        $newZip = new PositionZipcode();
                        $newZip->zipcode = $location->zipcode;
                        $newZip->city    = $location->city;
                        $newZip->state   = $location->state;
                        $newZip->latlon  = $location->latlon;
                        $newZip->save();
                    }
                }
                if($location->city && $location->state && $location->latlon) {
                    if (!PositionCity::where([['city',$location->city], ['state',$location->state]])->exists()) { //insert city
                        $newCity = new PositionCity();
                        $newCity->city    = $location->city;
                        $newCity->state   = $location->state;
                        $newCity->latlon  = $location->latlon;
                        $newCity->country = $location->country;
                        $newCity->save();
                    }
                }

                if($location->county && $location->city && $location->state) {
                    $county = PositionCounty::where([['county',$location->county],['state',$location->state]])->first();
                    if(!$county) {
                        $county = new PositionCounty();
                        $county->county  = $location->county;
                        $county->state   = $location->state;
                        $county->save();
                    }

                    $city = PositionCity::where([['city',$location->city],['state',$location->state]])->first();

                    if(!$county->positionCities->contains($city->id)) {
                        $county->positionCities()->attach($city->id);
                    }
                }
            }
        }
    }

}