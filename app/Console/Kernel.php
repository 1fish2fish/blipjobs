<?php

namespace App\Console;

use App\Blip\UsStates;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\IndeedJobs::class,
        Commands\MemoryLeak::class,
        Commands\PositionCountyInput::class,
        Commands\CareerJet::class,
        Commands\ScrapeJobs::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command("jobs:indeed")->twiceDaily(1,13)->withoutOverlapping()->sendOutputTo(storage_path('logs/indeed.log'));
        //$schedule->command("jobs:careerjet")->twiceDaily(2,14)->withoutOverlapping()->sendOutputTo(storage_path('logs/careerjet.log'));

        // Solr Data Import Delta
        $schedule->exec('curl http://' . config('solr.solr_host') . ':' . config('solr.solr_port') . config('solr.solr_path') . 'dataimport?command=delta-import')->everyFiveMinutes()->sendOutputTo(storage_path('logs/solr.log'));
    }
}