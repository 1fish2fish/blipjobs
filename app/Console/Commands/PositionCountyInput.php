<?php

namespace App\Console\Commands;

use App\Models\PositionCity;
use App\Models\PositionCounty;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class PositionCountyInput extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'input:counties {file}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Pull it in';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if(!file_exists($this->argument('file'))) {
            throwException('File Doesn\'t Exist');
        }

        $fh = fopen($this->argument('file'),'r');

        while($row = fgetcsv($fh)) {

            $city = PositionCity::where([   'city'  => $row[2],
                                            'state' => $row[0]])->first();

            $counties = explode(",", $row[5]);

            if($city) {
                foreach($counties as $countyString) {
                    $county = PositionCounty::firstOrCreate(['county' => $countyString, 'state' => $row[0]]);

                    DB::statement('  INSERT IGNORE INTO position_city_position_county
                                 (position_city_id, position_county_id)
                                 VALUES
                                 (' . $city->id . ',' . $county->id .')');
                }
            } else {
                $this->line("city not in db. " . $row[2] . ", " . $row[0]);
            }
        }
    }
}
