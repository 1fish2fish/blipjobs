<?php

namespace App\Console\Commands;

use App\Blip\Jobs\CareerJetAPI;
use App\Blip\UsStates;
use App\Models\JobPosting;
use App\Models\JobSource;
use App\Models\PositionCity;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;
use Goutte\Client as ScrapeClient;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\OutputInterface;

class CareerJet extends Command
{
    protected $job_source_id;

    protected $user_id;
    protected $scrape;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'jobs:careerjet {state?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Hits the CareerJet API and pulls all jobs.  Populates database table job_postings with new jobs.';

    public function __construct()
    {
        parent::__construct();

        $this->user   = User::where('email', 'drew.ostrowski@gmail.com')->first();
        $this->source = JobSource::firstOrCreate(["name" => "CareerJetAPI"]);
        $this->affid  = '7aed5008a94c4ebd0261f2dd05e5478c';
        $this->scrape = true;

        $this->stats = [
            'NoGeo' => 0,
            'Dupe' => 0,
            'Inserted' => 0,
            'Processed' => 0,
        ];
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $api = new CareerJetAPI('en_US');

        foreach($this->getStates() as $state_abr => $state) {

            $cities      = PositionCity::where("state", $state_abr)->get();
            foreach ($cities as $city) {

                $progressBar = $this->output->createProgressBar();
                $progressBar->setFormat("%message%\n %current%/%max% [%bar%] %percent:3s%% %elapsed% %memory%\n%stats%");
                $progressBar->setRedrawFrequency(1);
                $progressBar->setMessage("City: $city->city, $city->state");
                $progressBar->setMessage("","stats");
                $progressBar->setPlaceholderFormatterDefinition('message', function(ProgressBar $bar, OutputInterface $output) {
                    return $bar->getMessage();
                });
                $progressBar->setPlaceholderFormatterDefinition('stats', function(ProgressBar $bar, OutputInterface $output) {
                    return $bar->getMessage('stats');
                });
                $progressBar->start();
                $page = 0;
                do {
                    $page++;

                    $results = $api->search([
                        'keywords' => '',
                        'location' => $city->city . ", " . $city->state,
                        'affid' => $this->affid,
                        'page' => $page]);

                    if(isset($results->jobs) && count($results->jobs) > 0) {
                        $jobs = $this->resultsToJobs($results, $city);
                        $inserted = $this->jobsToDB($jobs);
                        $this->stats['Inserted'] += $inserted;
                        $this->stats['Processed'] += count($results->jobs);
                        $stats = "Last Inserted: " . $inserted . "\n";
                        $stats.= "Last Processed: " . count($results->jobs) . "\n";
                        $stats.= "Total Inserted: " . $this->stats['Inserted'] . "\n";
                        $stats.= "Total Processed: " . $this->stats['Processed'] . "\n";
                        $stats.= "Total Dupes: " . $this->stats['Dupe'] . "\n";
                        $progressBar->setMessage($stats,'stats');
                    }
                    $progressBar->advance();

                } while (isset($results->pages) && $page < $results->pages && $page < 51); // they stop giving the goods after 50 * 20 = 1000
                $progressBar->finish();
            }
            $this->line("\nSTATS");
            $this->line("-----");
            foreach($this->stats as $label => $result) {
                $this->line("$label : $result");
            }
        }
    }

    public function getStates()
    {
        if($this->argument('state')) {
            return [$this->argument('state') => $this->argument('state')];
        } else {
            return UsStates::getStates();
        }
    }

    public function resultsToJobs($results, $city)
    {
        $jobs = new Collection();

        foreach($results->jobs as $result) {

            $job = new JobPosting();

            $description = $this->scrapeDescription($result->url);

            $job->user_id         = $this->user->id;
            $job->company         = $result->company;
            $job->position        = $result->title;
            $job->description     = $description?$description:$result->description;
            $job->posted_at       = new Carbon($result->date);
            $job->created_at      = new Carbon();
            $job->posted          = 1;
            $job->job_source_id   = $this->source->id;
            $job->url             = $result->url;
            $job->location_city   = ucwords(strtolower($city->city));
            $job->location_state  = $city->state;
            $job->location_latlon = $city->latlon;

            $job->setJobSourceKey();

            $jobs->add($job);
        }

        return $jobs;
    }

    public function jobsToDB($jobs)
    {
        //remove duplicate jobs that match database rows
        $jobs    = $this->removeNonUnique($jobs);

        if($jobs->count() == 0) {
            return 0;
        }

        //insert jobs
        $values = $jobs->map(function ($job) {
            return $job->toArray();
        });

        DB::table('job_postings')->insert($values->all());

        return $values->count();
    }

    public function removeNonUnique($jobs)
    {
        $countBefore = $jobs->count();

        $jobs      = $jobs->keyBy('job_source_key');
        $dirtyKeys = DB::table('job_postings')->whereIn('job_source_key',$jobs->keys())->pluck('job_source_key');

        $jobs->forget($dirtyKeys);

        if($countBefore != $jobs->count()) {
            $this->stats['Dupe'] += ($countBefore - $jobs->count());
        }

        return $jobs;
    }

    public function scrapeDescription($url)
    {
        if(!$this->scrape) {
            return false;
        }

        $scraper = new ScrapeClient();
        sleep(2);

        $crawler = $scraper->request('GET', $url);

        if($scraper->getResponse()->getStatus() == 200) {

            //javascript redirect window.location sent. and set affid
            $url = substr($crawler->text(),strpos($crawler->text(),"http:"));
            $url = substr($url,0,-4);

            sleep(2);

            $crawler = $scraper->request('GET', $url . "?affid=" . $this->affid);

            if($scraper->getResponse()->getStatus() == 200) {
                $descriptions = $crawler->filter('.advertise_compact');

                return $descriptions->text();
            }
        }

        $this->scrape = false;

        return false;
    }
}
