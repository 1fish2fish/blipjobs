<?php

namespace App\Console\Commands;

use App\Blip\Jobs\JobScraper;
use App\Blip\Jobs\JobScraperConfig;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\OutputInterface;

class ScrapeJobs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'jobs:scrape {config}';

    protected $stats;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Scrape Jobs from site based on config';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->stats = [
            'NoGeo' => 0,
            'Dupe' => 0,
            'Inserted' => 0,
            'Processed' => 0,
        ];

        $config   = $this->getConfig();
        $scraper  = new JobScraper($config);
        $scraper->setURL($config->url);
        $scraper->modifySearch();


        $progressBar = $this->output->createProgressBar();
        $progressBar->setRedrawFrequency(1);
        $progressBar->setFormat("%current% [%bar%] %elapsed% %memory%\n\n%stats%");
        $progressBar->setMessage("","stats");
        $progressBar->setPlaceholderFormatterDefinition('stats', function(ProgressBar $bar, OutputInterface $output) {
            return $bar->getMessage('stats');
        });
        $progressBar->start();

        do {
            $postings = $scraper->scrapePostingsFromPage();
            $inserted = $this->jobsToDB($postings);

            $this->stats['Inserted'] += $inserted;
            $this->stats['Processed'] += $postings->count();
            $stats = "Last Inserted: " . $inserted . "\n";
            $stats.= "Last Processed: " . $postings->count() . "\n";
            $stats.= "Total Inserted: " . $this->stats['Inserted'] . "\n";
            $stats.= "Total Processed: " . $this->stats['Processed'] . "\n";
            $stats.= "Total Dupes: " . $this->stats['Dupe'] . "\n";
            $progressBar->setMessage($stats,'stats');

            $nextPage = $scraper->getNextPage();

            $progressBar->advance();

        } while($nextPage);

        $progressBar->finish();
    }

    public function jobsToDB($jobs)
    {
        //remove duplicate jobs that match database rows
        $jobs    = $this->removeNonUnique($jobs);

        if($jobs->count() == 0) {
            return 0;
        }

        //insert jobs
        $values = $jobs->map(function ($job) {
            return $job->toArray();
        });

        DB::table('job_postings')->insert($values->all());

        return $values->count();
    }

    public function removeNonUnique($jobs)
    {
        $countBefore = $jobs->count();

        $jobs      = $jobs->keyBy('job_source_key');
        $dirtyKeys = DB::table('job_postings')->whereIn('job_source_key',$jobs->keys())->pluck('job_source_key');

        $jobs->forget($dirtyKeys);

        if($countBefore != $jobs->count()) {
            $this->stats['Dupe'] += ($countBefore - $jobs->count());
        }

        return $jobs;
    }

    public function getConfig()
    {
        //TODO: use this to find proper configs (Hard Coding For Testing)
        $configName = $this->argument('config');

        $this->line("Processing Config $configName\n");

        $config = new JobScraperConfig();

        $config->url          = "https://www.google.com/about/careers/search";
        $config->company      = "Google";
        $config->postingsPart = "a.title";
        $config->positionPart = "span[itemprop='name title']";
        $config->locationPart = "span[itemprop='name']";
        $config->descriptionPart = [
            "div[itemprop='description']",
            "div[itemprop='responsibilities']",
            "div.tagstory"
        ];
        $config->postedDatePart = "div[itemprop='datePosted']";
        $config->nextPage = ".pages a[title='Next']:not(.disabled)";

        $config->modSearch      = [
            ['type' => 'xpath', 'search' => '//a[text()="View locations"]'], // Get this right, it's picking 2 needs to pick 1 Try Xpath?
            ['type' => 'css', 'search' => '.US .fakecheckbox'],
            ['type' => 'css', 'search' => '.kd-modaldialog a.kd-button.primary'],
        ];

        $config->back = ".appbar-main .kd-buttonbar a[title='Back to search results']";

        return $config;
    }
}
