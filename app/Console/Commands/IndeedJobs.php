<?php

namespace App\Console\Commands;

use App\Blip\Jobs\IndeedAPI;
use App\Blip\UsStates;
use App\Models\JobPosition;
use App\Models\PositionZipcode;
use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class IndeedJobs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'jobs:indeed {state?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Hits the Indeed API and pulls all jobs since last pull.  Populates database table job_postings with new jobs.';

    protected $indeedApi;

    protected $stats;

    const MAX_SALARY = 30000;

    const LIMIT = 25;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        //blip job type => indeed job type
        $this->jobPositions = JobPosition::get();
        $this->user         = User::where('email', 'drew.ostrowski@gmail.com')->first();

        $this->stats  = [
            'NoGeo' => 0,
            'Dupe'  => 0,
            'Inserted'  => 0,
            'Processed' => 0,
        ];

        $this->indeedApi = new IndeedAPI([
            'publisher' => '4653291213537220',
            'v'         => 2,
            'highlight' => 0,
            'limit'     => self::LIMIT,
            'format'    => 'json',
            'radius'    => 0,
        ]);
        $this->indeedApi
            ->setDaysBack(2)
            ->setUserIp('192.168.0.32')
            ->setUserAgent('Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36')
            ->setUserId($this->user->id);
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if($this->argument('state')) {
            $states = [$this->argument('state') => $this->argument('state')];
        } else {
            $states = UsStates::getStates();
        }

        foreach($states as $state_abr => $state) {

            $this->locations = PositionZipcode::where("state",$state_abr)->get();

            foreach($this->jobPositions as $jobPosition) {
                $this->line("Pulling $jobPosition->name Jobs From $state ");

                //get count
                $this->indeedApi
                    ->setJobType($jobPosition->indeed_name)
                    ->setLocation($state_abr)
                    ->setCount(1)
                    ->getJobs();

                $this->line("Result Count: " . $this->indeedApi->getResultCount());

                $progressBar = $this->output->createProgressBar($this->locations->count());
                $this->indeedApi->setCount(25);

                if($this->indeedApi->getResultCount() > 0) {

                    foreach($this->locations as $location) {
                        $this->currentPosition = $jobPosition;

                        $this->pullJobs([
                                'indeedJobType' => $jobPosition->indeed_name,
                                'location'      => $location->zipcode,
                                'position'      => $location,
                            ]
                        );

                        $progressBar->advance();
                    }

                    $progressBar->finish();


                    $this->line("\nSTATS");
                    $this->line("-----");
                    foreach($this->stats as $label => $result) {
                        $this->line("$label : $result");
                    }

                    $this->line("Memory Usage: " . memory_get_usage(false));
                }
            }
        }
    }

    public function pullJobs($options)
    {
        $processed    = 0;

        $this->indeedApi
            ->setJobType($options['indeedJobType'])
            ->setLocation($options['location']);

        if(isset($options['position'])) {
            $this->indeedApi->setPosition($options['position']);
        }

        do {
            $page = $this->indeedApi->getNext();
            $jobs = $this->indeedApi->setPage($page)->getJobs();

            if(!isset($resultCount)) {
                $resultCount = $this->indeedApi->getResultCount();
            }

            if($jobs->count()) {
                $this->stats['Inserted']  += $this->jobsToDB($jobs,$options['location']);
                $this->stats['Processed'] += $jobs->count();
                $processed += $jobs->count();

                if($this->stats['Processed'] != $this->stats['NoGeo'] + $this->stats['Dupe'] + $this->stats['Inserted']) {
                    $this->line("don't compute");
                }
            }

            usleep(200);

        } while($processed < $resultCount);

        if($resultCount > 1000) {
            $this->line("We got a problem. result count > 1000");
            $this->line("results:   $resultCount");
        }
    }

    public function jobsToDB($jobs)
    {
        //remove duplicate jobs that match database rows
        $jobs    = $this->removeNonUnique($jobs);

        if($jobs->count() == 0) {
            return 0;
        }

        //insert jobs
        $values = $jobs->map(function ($job) {
                    return $job->toArray();
            });

        DB::table('job_postings')->insert($values->all());

        $max = DB::table('job_postings')->select('id')->orderBy('id','desc')->limit(1)->get();

        $positionMap = [];

        for( $i = 0; $i < count($values); $i++) {
            $positionMap[] = [
                'job_posting_id' => $max[0]->id - $i,
                'job_position_id' => $this->currentPosition->id
            ];
        }

        DB::table('job_position_job_posting')->insert($positionMap);

        return $values->count();
    }

    public function removeNonUnique($jobs)
    {
        $countBefore = $jobs->count();

        $jobs      = $jobs->keyBy('job_source_key');
        $dirtyKeys = DB::table('job_postings')->whereIn('job_source_key',$jobs->keys())->pluck('job_source_key');

        $jobs->forget($dirtyKeys);

        if($countBefore != $jobs->count()) {
            $this->stats['Dupe'] += ($countBefore - $jobs->count());
        }

        return $jobs;
    }

    public function updatePositions($jobs) {
        $updates = [];

        foreach($jobs as $job) {
            $updates[] = "($job->id," . $this->currentPosition->id . ")";
        }

        DB::statement("INSERT IGNORE INTO job_position_job_posting
                       (job_posting_id,job_position_id)
                       VALUES
                       " . implode(",",$updates));
    }
}
