<?php

namespace App\Jobs;

use App\Jobs\Job;
use App\Models\JobApplication;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class SendApplicationEmail extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $app;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(JobApplication $app)
    {
        $this->app = $app;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $jobApp = $this->app;

        $path = storage_path();

        Mail::send('emails.application',['jobApp' => $this->app], function($m) use ($jobApp) {
            $m->from('admin@blipjobs.com', 'Blipjobs');
            $m->to($jobApp->jobPostings->contact,$jobApp->jobPostings->contact);
            $m->subject('New Applicant. ' . $jobApp->jobPostings->company . " " . $jobApp->jobPostings->position);
            $m->attachData(Storage::disk('local')->get($jobApp->userResumes->getSystemPath()),$jobApp->userResumes->user_file_name);

        });
    }

    public function failed(\Exception $e)
    {
        echo $e->getMessage();
    }

}
