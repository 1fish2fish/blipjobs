<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JobView extends Model
{
    protected $fillable = ['job_posting_id', 'user_id'];

    public function jobPostings()
    {
        return $this->belongsTo(JobPosting::class);
    }

    public function users()
    {
        return $this->belongsTo(User::class);
    }
}
