<?php

namespace App\Models;

use App\Blip\GeoPosition;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class PositionCity extends Model
{
    use GeoPosition;

    protected $geofields = array('latlon');

    protected $table = 'position_city';

    public $timestamps = false;

    public function positionCounty()
    {
        $this->belongsToMany(PositionCounty::class, 'position_city_position_county', 'position_city_id', 'position_county_id');
    }

}
