<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class UserResume extends Model
{

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function jobApplications()
    {
        return $this->hasMany(JobApplication::class);
    }

    public function getNewSystemFileName($ext)
    {
        return $this->getHexFileName('system_file_name',$ext);
    }

    public function getNewImageFileName($ext)
    {
        return $this->getHexFileName('image_file_name', $ext);
    }

    public function getHexFileName($column, $ext)
    {
        do {

            $filename = md5(uniqid()) . ".$ext";

        } while ($this::where([$column => $filename])->get()->count() > 0);

        return $filename;
    }

    public function getResumeFolder()
    {
        $folder         = substr($this->system_file_name,0,2);
        $subfolder      = substr($this->system_file_name,2,2);

        return "user/resumes/$folder/$subfolder";
    }

    public function getSystemPath() {
        return $this->getResumeFolder() . "/" . $this->system_file_name;
    }

}
