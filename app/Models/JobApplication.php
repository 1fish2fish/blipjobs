<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JobApplication extends Model
{
    protected $fillable = ['user_resume_id','user_cover_letter_id','job_posting_id'];

    public function jobPostings()
    {
        return $this->belongsTo(JobPosting::class,'job_posting_id');
    }

    public function userResumes()
    {
        return $this->belongsTo(UserResume::class,'user_resume_id');
    }

    public function userCoverLetters()
    {
        return $this->belongsTo(UserCoverLetter::class,'user_cover_letter_id');
    }

    public function users()
    {
        return $this->belongsTo(User::class,'user_id');
    }
}
