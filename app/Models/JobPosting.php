<?php

namespace App\Models;

use App\Blip\GeoPosition;
use Illuminate\Database\Eloquent\Model;

class JobPosting extends Model
{
    use GeoPosition;

    protected $geofields = array('location_latlon');

    public function getTargetAttribute()
    {
        if($this->job_source_id == JobSource::where('name','BlipJobs')->first()->id) {
            return "_self";
        } else {
            return "_blank";
        }
    }

    public function getUrlAttribute($value)
    {
        if($this->job_source_id == JobSource::where('name','BlipJobs')->first()->id) {
            return "/job/" . $this->id;
        } else {
            return $value;
        }
    }

    public function getSnippet()
    {
        return substr(strip_tags($this->description),0,200)  . '...';
    }

    public function setJobSourceKey()
    {
        $this->job_source_key = sha1($this->company . $this->position .
            $this->location_city . $this->location_state );
    }


    public function jobApplications()
    {
        return $this->hasMany(JobApplication::class);
    }

    public function jobPositions()
    {
        return $this->belongsToMany('App\Models\JobPosition', 'job_position_job_posting');
    }

    public function jobPostingExperienceReq()
    {
        return $this->hasMany('App\Models\JobPostingExperienceReq');
    }

    public function jobPostingLanguageReq()
    {
        return $this->hasMany('App\Models\JobPostingLanguageReq');
    }

    public function jobPostingLicenseReq()
    {
        return $this->hasMany('App\Models\JobPostingLicenseReq');
    }

    public function jobSource()
    {
        return $this->belongsTo('App\Models\JobSource');
    }

    public function jobView()
    {
        return $this->hasMany(JobView::class);
    }

    public function jobViewStat()
    {
        return $this->hasOne(JobViewStat::class, 'job_posting_id');
    }

    public function jobApplicationStat()
    {
        return $this->hasOne(JobApplicationStat::class, 'job_posting_id');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

}
