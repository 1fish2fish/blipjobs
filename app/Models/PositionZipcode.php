<?php

namespace App\Models;

use App\Blip\GeoPosition;
use Illuminate\Database\Eloquent\Model;

class PositionZipcode extends Model
{
    use GeoPosition;

    protected $geofields = array('latlon');

    protected $table = 'position_zipcode';

    public $timestamps = false;

    public function equals($city,$state,$zip)
    {
        if(strtoupper($city) != $this->city) {
            return false;
        }
        if(strtoupper($state) != $this->state) {
            return false;
        }
        if(strtoupper($zip) != $this->zipcode) {
            return false;
        }

        return true;
    }

}
