<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserContact extends Model
{

    protected $table = 'user_contact';

    protected $primaryKey = 'user_id';

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
