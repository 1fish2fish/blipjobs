<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait;

class User extends Authenticatable
{
    use EntrustUserTrait;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getCoverLetterArray()
    {
        $coverArray = [ '0' => 'No Cover Letter'];

        foreach($this->coverLetters as $cover) {
            $coverArray[$cover->id] = $cover->name;
        }

        return $coverArray;
    }

    public function getDefaultCoverLetter()
    {
        foreach($this->coverLetters as $cover) {
            if($cover->default == 1) {
                return $cover->id;
            }
        }

        return false;
    }

    public function getResumesArray()
    {
        $resumeArray = [];

        foreach($this->resumes as $resume) {
            $resumeArray[$resume->id] = $resume->name;
        }

        if(count($resumeArray) == 0) {
            $resumeArray['0'] = "No Resumes.";
        }

        return $resumeArray;
    }

    public function getDefaultResume()
    {
        foreach($this->resumes as $resume) {
            if($resume->default == 1) {
                return $resume->id;
            }
        }

        return false;
    }

    public function userContact()
    {
        return $this->hasOne(UserContact::class);
    }

    public function resumes()
    {
        return $this->hasMany(UserResume::class);
    }

    public function coverLetters()
    {
        return $this->hasMany(UserCoverLetter::class);
    }

    public function jobStatuses()
    {
        return $this->hasMany(UserJobStatus::class);
    }

    public function jobPostings()
    {
        return $this->hasMany(JobPosting::class);
    }

    public function jobApplications()
    {
        return $this->hasMany(JobApplication::class);
    }

    public function jobView()
    {
        return $this->hasMany(JobView::class);
    }

    public function UserJobPostingStat()
    {
        return $this->hasOne(UserJobPostingStat::class, "user_id");
    }
}
