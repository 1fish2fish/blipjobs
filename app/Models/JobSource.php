<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JobSource extends Model
{
    protected $fillable = [
        'name'
    ];

    public function jobPostings() {
        return $this->hasMany(JobPosting::class);
    }

    public static function getSourceSelect() {
        $sources = JobSource::all();
        $selectArray = [0 => 'All'];

        foreach($sources as $source) {
            $selectArray[$source->id] = $source->name;
        }

        return $selectArray;
    }
}
