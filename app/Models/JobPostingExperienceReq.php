<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JobPostingExperienceReq extends Model
{
    protected $table = 'job_posting_experience_req';

    public $timestamps = false;

    public function jobPosting()
    {
        $this->belongsTo(JobPosting::class);
    }
}
