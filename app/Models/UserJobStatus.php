<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserJobStatus extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'job_posting_id', 'status'
    ];

    public function jobSource() {
        return $this->hasOne(JobSource::class);
    }

    public function user() {
        return $this->hasOne(User::class);
    }
}
