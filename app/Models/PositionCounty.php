<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PositionCounty extends Model
{
    protected $table = 'position_county';

    protected $fillable = ['county','state'];

    public $timestamps = false;

    public function positionCities()
    {
        return $this->belongsToMany(PositionCity::class, 'position_city_position_county', 'position_county_id', 'position_city_id');
    }

}