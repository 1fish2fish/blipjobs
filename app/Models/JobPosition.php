<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JobPosition extends Model
{
    public function jobPostings()
    {
        $this->belongsToMany('App\Models\JobPosting','job_position_job_posting');
    }
}
