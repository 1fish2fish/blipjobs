<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JobApplicationStat extends Model
{
    protected $primaryKey = null;

    public $incrementing = false;

    public function jobPostings()
    {
        return $this->belongsTo(JobPosting::class,'job_posting_id');
    }
}