<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JobPostingLicenseReq extends Model
{
    protected $table = 'job_posting_license_req';

    public $timestamps = false;

    public function jobPosting()
    {
        $this->belongsTo(JobPosting::class);
    }
}
