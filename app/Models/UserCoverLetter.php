<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserCoverLetter extends Model
{
    public function jobApplications()
    {
        return $this->hasMany(JobApplication::class);
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
