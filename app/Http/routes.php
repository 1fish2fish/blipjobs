<?php


/*
 * User Job Poster
 *
 * This is the User Portion of the site.
 * User can edit their properties.
 * But only their properties.
 */
Route::group([
    'middleware' => ['auth', 'role:super-admin|job-poster'],

], function() {

    Route::get('files/user/resumes/{systemFileName}','ResumeController@download');
    Route::get('files/user/cover-letter/{id}','UserCoverLetterController@download');

    Route::get('job-applications/{id}','JobApplicationController@show');
    Route::get('job/admin','JobPostingController@admin');
    Route::resource('job','JobPostingController',['except' => 'show']);

    Route::get('job/{id}/requirements',['as' => 'job.requirement.create', 'uses' => 'JobPostingController@createRequirements']);
    Route::post('job/{id}/requirements',['as' => 'job.requirement.store', 'uses' => 'JobPostingController@storeRequirements']);
    Route::get('job/{id}/requirements/edit',['as' => 'job.requirement.edit', 'uses' => 'JobPostingController@editRequirements']);
    Route::put('job/{id}/requirements',['as' => 'job.requirement.update', 'uses' => 'JobPostingController@updateRequirements']);

});

/*
 * Public
 *
 * This is the Public Portion of the site.
 */
Route::get('/', 'StaticController@home');
Route::get('/privacy', 'StaticController@privacyPolicy');
Route::get('/terms', 'StaticController@termsOfUse');
Route::get('/search',['as' => 'job-search', 'uses' => 'JobSearchController@index']);
Route::get('/data/city-state', 'PositionController@ajaxGetCityState');
Route::auth();
Route::get('job/{id}',['as' => 'job.show', 'uses' => 'JobPostingController@show']);

/*
 * Movology Test
 */
Route::get("/movology/source", "MovologyController@source");
Route::get("/movology",'MovologyController@index');
Route::post("/movology",'MovologyController@index');

/*
 * Admin
 *
 * This is the Admin Portion of the site.
 * Access is restricted here.
 * admin only.
 */

Route::group([
    'middleware' => ['auth', 'role:admin|super-admin'],
    'prefix'     => 'admin'
], function() {

    Route::post('ajax/job-publish','JobPostingController@publish');
    Route::post('ajax/job-unpublish','JobPostingController@unpublish');

    Route::get('user/roles','RoleController@index');
    Route::resource('user','UserController');
    Route::resource('roles','RoleController');

});

/*
 * Ajax
 *
 * This is the Ajax Portion of the site.
 * Ajax calls are routed here.
 */
Route::group([
    'middleware' => 'auth',
    'prefix'     => 'ajax'
], function() {


    Route::post('job/apply','JobApplicationController@ajaxApply');
    Route::put('user/cover-letters/{cover_letters}','UserCoverLetterController@ajaxUpdate');
    Route::delete('user/cover-letters/{cover_letters}','UserCoverLetterController@ajaxDestroy');

    Route::resource('user/job-status','UserJobStatusController', ['only' => 'store','show','update','destroy']);
});

/*
 * User (Any Registered User)
 *
 * This is the User Portion of the site.
 * User can edit their properties.
 * But only their properties.
 */
Route::group([
    'middleware' => 'auth',
    ], function() {

    Route::get('apply/{id}','JobApplicationController@apply');

    Route::get('user/profile','UserController@profile');
    Route::post('user/profile','UserController@profileUpdate');

    Route::resource('user/resumes','ResumeController');
    Route::resource('user/cover-letters','UserCoverLetterController');

});

