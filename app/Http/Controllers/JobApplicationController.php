<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Jobs\SendApplicationEmail;
use App\Models\JobApplication;
use App\Models\JobPosting;
use App\Models\UserResume;
use Illuminate\Http\Request;

class JobApplicationController extends Controller
{
    public function ajaxApply(Request $request)
    {
        if(!UserResume::find($request->input('user_resume_id'))) {
            return response()->json(['error' => 'Resume either not found or not provided.<br/> If you have not added a resume, go to your profile to add.<br/> Resume is required.', 'resume' => 'Resume is Required.'], 404);
        }

        if(!JobPosting::find($request->input('job_posting_id'))) {
            return response()->json(['error' => 'Job Posting Not Found.', 'job_posting_id' => 'Job Posting is Required.'], 404);
        }

        $jobApp = new JobApplication($request->all());
        $jobApp->user_id = $request->user()->id;
        $jobApp->save();

        //Add Job to Queue.  Email job poster with applicant's info.
        $this->dispatch(new SendApplicationEmail($jobApp));

        return ['success'];
    }

    public function show(Request $request, $id)
    {
        $applications = JobApplication::where("job_posting_id", $id)
            ->with('users.userContact')
            ->with('userResumes')
            ->with('userCoverLetters')
            ->paginate(10);

        return view('pages.job.application.list', ['applications' => $applications]);
    }
}