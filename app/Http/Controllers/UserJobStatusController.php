<?php

namespace App\Http\Controllers;

use App\Models\JobSource;
use App\Models\JobView;
use App\Models\UserJobStatus;
use Illuminate\Http\Request;

use App\Http\Requests;

class UserJobStatusController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = [
            'user_id' => $request->user()->id,
            'job_posting_id' => $request->input('job_id'),
        ];

        $jobStatus = UserJobStatus::firstOrNew($data);

        $jobStatus->status = $request->input('status');
        $jobStatus->save();

        if($request->input('status') == 'viewed') {
            $jobView = new JobView($data);
            $jobView->user_id = $request->user()->id;
            $jobView->save();
        }

        return 'success';
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
