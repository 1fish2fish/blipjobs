<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Log;

/**
 * Movology Test
 *
 * Resume submission api from Movology to test programming apptitude
 * Answer Questions based on query string from javascript code
 * Question are in the ?q= parameter. example 'Ping'...
 *
 * User: Drew Ostrowski
 * Date: 10/20/2016
 * Time: 7:51 PM
 */
class MovologyController extends Controller
{
    /**
     * Reqex for q parameter validation and question parsing.
     *
     * @var string
     */
    private $regexQ;

    /**
     * construct
     */
    public function __construct()
    {
        $this->regexQ = '/^\[\\\'q\\\'\]\[\\\'(.*)\\\'\]$/';
    }


    /**
     * Validates, question and if possible Answers resume question
     *
     * input parameter ?q= : question to answer. example 'Ping
     *
     * @param Request $request
     * @return bool|string
     */
    public function index(Request $request)
    {

        $validator = Validator::make($_GET,[
            'q' => "required|regex:$this->regexQ",
        ]);

        //Error can't parse the question (400)
        if($validator->fails()) {
            $error = 'Can\'t parse the question should be formated [\'q\'][\'...Question...\']';
            Log::error($_POST);
            Log::error($_GET);
            Log::error($error);
            return response()->view('errors.plain',[
                'response' => $error], 400);
        }

        $answer = $this->answerQuestion($_GET['q'],$_GET['puzzle']);

        //Error no question found that is answerable (400)
        if(!$answer) {
            $error = 'Question is not answerable' . "\n" . 'Submited question q=';
            Log::error($_POST);
            Log::error($_GET);
            Log::error($error);
            return response()->view('errors.plain',[
                'response' => $error], 400);
        }

        Log::error($_POST);
        Log::error($_GET);
        Log::error($answer);

        return $answer;
    }

    /**
     * View source code
     *
     * @param Request $request
     */
    public function source(Request $request)
    {
        $sourceFile = file_get_contents(app_path() . '/Http/Controllers/MovologyController.php');

        return view('movology', ['sourceFile' => $sourceFile]);
    }

    /**
     * Returns answer of inputed question or false if not found.
     *
     * @param $rawQuestion
     * @return bool|string
     */
    private function answerQuestion($rawQuestion,$puzzle)
    {
        $question = $this->parseQuestion($rawQuestion);

        switch($question) {
            case "":
                return false;
            case 'Ping' :
                return "OK";
            case 'Name' :
                return 'Drew Ostrowski';
            case 'Phone':
                return '949-903-6917';
            case 'Years':
                return '9 Years';
            case 'Referrer':
                return 'recruiter';
            case 'Source':
                return 'http://www.blipjobs.com/movology/source';
            case 'Email':
                return 'drew.ostrowski@gmail.com';
            case 'Resume':
                return 'https://www.dropbox.com/s/2mbpxyqgqb952p3/Drew%20Ostrowski%20Resume.docx?dl=0';
            case 'Position':
                return 'Sr Software Engineer';
            case 'Status':
                return 'US Citizen';
            case 'Puzzle':
                $answer = 0;
                try {
                    eval('$answer = ' . base64_decode($puzzle) . ';');
                    $answer = number_format($answer,0);
                } catch(Exception $e) {
                    return false;
                }

                return $answer;
            default:
                return false;
        }
    }

    /**
     * Parses Question
     *
     * @param $rawQuestion
     * @return string
     */
    private function parseQuestion($rawQuestion)
    {
        if(preg_match($this->regexQ, $rawQuestion, $matches)) {
            return $matches[1];
        } else {
            return "";
        }
    }

}