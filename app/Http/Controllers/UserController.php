<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\UserContact;
use App\Models\Auth\Role;
use Illuminate\Http\Request;
use Auth;

use App\Http\Requests;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = User::orderBy('id','DESC')->paginate(5);
        return view('pages.admin.user.index',compact('data'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::lists('display_name','id');
        return view('pages.admin.user.create',compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email|unique:users,email',
            'password' => 'required|same:confirm-password',
            'roles' => 'required'
        ]);

        $input = $request->all();
        $input['password'] = bcrypt($input['password']);

        $user = User::create($input);
        foreach ($request->input('roles') as $key => $value) {
            $user->attachRole($value);
        }

        return redirect()->route('admin.user.index')
            ->with('success','User created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        return view('pages.admin.user.show',compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        $roles = Role::lists('display_name','id');
        $userRole = $user->roles->lists('id','id')->toArray();

        return view('pages.admin.user.edit',compact('user','roles','userRole'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'email' => 'required|email|unique:users,email,'.$id,
            'password' => 'same:confirm-password',
            'roles' => 'required'
        ]);

        $input = $request->all();
        if(!empty($input['password'])){
            $input['password'] = Hash::make($input['password']);
        }else{
            $input = array_except($input,array('password'));
        }

        $user = User::find($id);
        $user->update($input);
        $user->detachRoles();


        foreach ($request->input('roles') as $key => $value) {
            $user->attachRole($value);
        }

        return redirect()->route('admin.user.index')
            ->with('success','User updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::find($id)->delete();
        return redirect()->route('admin.user.index')
            ->with('success','User deleted successfully');
    }

    public function profile()
    {
        if(!$contact = Auth::user()->userContact) {
            $contact = new UserContact();
        }

        return view('pages.user.profile',['contact' => $contact]);
    }

    public function profileUpdate(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email|unique:users,email,' . Auth::user()->id
        ]);

        if(!$contact = Auth::user()->userContact) {
            $contact = new UserContact();
        }

        $contact->address  = $request->input('address');
        $contact->address2 = $request->input('address2');
        $contact->city     = $request->input('city');
        $contact->state    = $request->input('state');
        $contact->zip      = $request->input('zip');
        $contact->phone    = $request->input('phone');
        $contact->user_id  = Auth::user()->id;
        $success = $contact->save();

        if($success) {
            Auth::user()->first_name = $request->input('first_name');
            Auth::user()->last_name  = $request->input('last_name');
            Auth::user()->email      = $request->input('email');
            $success = Auth::user()->save();
        }

        $request->session()->flash('success', 'Profile Updated.');

        return view('pages.user.profile', ['contact' => $contact]);
    }
}
