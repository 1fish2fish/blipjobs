<?php

namespace App\Http\Controllers;

use App\Models\JobPosting;
use Illuminate\Http\Request;

use App\Http\Requests;

class StaticController extends Controller
{
    public function home(Request $request)
    {
        //TODO: separate by role

        $jobPostings = [];

        if($request->user() && $request->user()->hasRole('job-poster')) {
            $jobPostings = JobPosting::where("user_id", "=", $request->user()->id)->paginate(5);
        }

        return view('pages.home',["jobPostings" => $jobPostings]);
    }

    public function privacyPolicy()
    {
        return view('pages.privacyPolicy');
    }

    public function termsOfUse()
    {
        return view('pages.termsOfUse');
    }
}
