<?php

namespace App\Http\Controllers;

use App\Models\UserCoverLetter;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

use App\Http\Requests;

class UserCoverLetterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return view('pages.user.cover-letters.index',['coverLetters' => $request->user()->coverLetters ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.user.cover-letters.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'cover-letter-name' => 'bail|required|unique:user_cover_letters,name,NULL,id,user_id,' . $request->user()->id,
            'cover-letter' => 'bail|required',
        ],
            [
                'cover-letter-name.unique' => 'You already have a resume with that name.',
            ]);

        $coverLetter = new UserCoverLetter();
        $coverLetter->name = $request->input('cover-letter-name');
        $coverLetter->cover_letter = $request->input('cover-letter');
        $coverLetter->default      = $request->input('default')?1:0;
        $coverLetter->user_id      = $request->user()->id;

        //Set other cover letter default to 0
        if($coverLetter->default == 1) {
            UserCoverLetter::where('user_id', $request->user()->id)->update(['default' => 0]);
        }

        $coverLetter->save();

        return view('pages.user.cover-letters.index', ['coverLetters' => $request->user()->coverLetters]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $coverLetter = UserCoverLetter::findOrFail($id);

        if($coverLetter->user_id == $request->user()->id) {
            return view('pages.user.cover-letters.show', ['coverLetter' => $coverLetter]);
        } else {
            return response('Unauthorized.', 401);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $coverLetter = UserCoverLetter::findOrFail($id);

        if($coverLetter->user_id == $request->user()->id) {
            return view('pages.user.cover-letters.edit', ['coverLetter' => $coverLetter]);
        } else {
            return response('Unauthorized.', 401);
        }
    }

    public function update(Request $request, $id)
    {
        $coverLetter = UserCoverLetter::findOrFail($id);

        if($coverLetter->user_id == $request->user()->id) {

            if($request->input('default')  == 1) {
                UserCoverLetter::where('user_id', $request->user()->id)->update(['default' => 0]);
            }

            $coverLetter->default      = $request->input('default')?1:0;
            $coverLetter->name         = $request->input('cover-letter-name');
            $coverLetter->cover_letter = $request->input('cover-letter');
            $coverLetter->save();

            $request->session()->flash('success', 'Cover Letter Updated.');

            return view('pages.user.cover-letters.show', ['coverLetter' => $coverLetter]);

        } else {
            return response('Unauthorized.', 401);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function ajaxUpdate(Request $request, $id)
    {
        $coverLetter = UserCoverLetter::findOrFail($id);

        if($coverLetter->user_id == $request->user()->id) {
            UserCoverLetter::where('user_id', $request->user()->id)->update(['default' => 0]);
            $coverLetter->default = 1;
            $coverLetter->save();

            return ['success' => true];
        } else {
            return response('Unauthorized.', 401);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function ajaxDestroy(Request $request, $id)
    {
        $coverLetter = UserCoverLetter::find($id);

        if($coverLetter->user_id == $request->user()->id) {
            if($coverLetter->delete()) {
                return ['success' => true];
            } else {
                abort(500,"Server Error");
            }
        } else {
            abort(403, "Unauthorized Action.");
        }
    }

    public function download($id)
    {
        $coverLetter = UserCoverLetter::find($id)->first();

        return response($coverLetter->cover_letter,200,['content-type' => 'text/plain']);
    }
}
