<?php

namespace App\Http\Controllers;

use App\Blip\Geocode;
use App\Models\JobPosting;
use App\Models\PositionCounty;
use App\Models\UserJobStatus;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Mockery\CountValidator\Exception;
use Solarium\Client as Solr;

class JobSearchController extends Controller
{
    public function index(Request $request)
    {
        $style = [
            'viewed'         => 'btn-info',
            'applied'        => 'btn-success',
            'interview'      => 'btn-success',
            'offer'          => 'btn-success',
            'not-interested' => 'btn-warning'
        ];

        try {
            $jobs = $this->getJobs($request);
        } catch(Exception $e) {
            return view('pages.search.list', [
                'jobs'     => new Collection(),
                'input'    => $request->input()])->withErrors($e->getMessage());
        }

        if($request->user()) {

            $jobIds = [];

            foreach($jobs->all() as $job) {
                $jobIds[] = $job->id;
            }

            $jobStatuses = UserJobStatus::where("user_id", $request->user()->id)
                ->whereIn("job_posting_id", $jobIds)
                ->get();

            foreach($jobStatuses->all() as $status) {
                $job = $jobs->filter(function($item) use ($status) {
                    return $item->id == $status->job_posting_id;
                })->first();
                if($job) {
                    $job->status = ucfirst($status->status);
                    $job->statusStyle = $style[$status->status];
                }
            }
        }

        return view('pages.search.list', [
            'jobs'     => $jobs,
            'input'    => $request->input()]);
    }

    public function getJobs($request)
    {
        $solr_config =  [   'endpoint' => [
                                'blipjobs' => [
                                    'host' => config('solr.solr_host'),
                                    'port' => config('solr.solr_port'),
                                    'path' => config('solr.solr_path')
                                ]
                            ]
                        ];

        $solr = new Solr($solr_config);

        $query  = $solr->createSelect();
        $helper = $query->getHelper();

        $query->setRows(10);
        $query->setStart(($request->input('page',1)-1)*10);

        if($request->input('l')) {
            $locations = Geocode::getPosition($request->input('l'));
        } else {
            $locations = [];
        }

        if(count($locations) == 0) {
            throw new Exception('Could not find location (' . $request->input('l') . ').  Please type in valid {city, state}, {county, state}, {zipcode} or {state} in the USA.');
        }
        if(count($locations) > 1) {
            //? throw error with multiple locations ?
        }

        $location = $locations[0];

        if($location->zipcode || $location->city) { // radius accepted
            if($request->input('r',5) > 0) {
                $query->createFilterQuery('location')->setQuery(
                    $helper->geofilt(
                        'company_latlon',
                        $location->getLat(),
                        $location->getLon(),
                        $request->input('r',5) * 1.609344 //km to miles
                    )
                );
            } else {
                if($location->zipcode) {
                    $query->createFilterQuery('location_zip')->setQuery("location_zip:$location->zipcode");
                } else {
                    $query->createFilterQuery('location_city')->setQuery("location_city:\"$location->city\"");
                    $query->createFilterQuery('location_state')->setQuery("location_state:$location->state");
                }
            }
        } else { // search based on text (state or county)
            if($location->county && $location->state) {
                //get all cities in county.
                $county = PositionCounty::where([['county',$location->county],['state', $location->state]])->with('positionCities')->first();
                $cities = implode("\" OR \"",$county->positionCities->lists('city')->toArray());
                $query->createFilterQuery('location_city')->setQuery("location_city:(\"$cities\")");
                $query->createFilterQuery('location_state')->setQuery("location_state:$location->state");

            } else if($location->state) {
                $query->createFilterQuery('location_state')->setQuery("location_state:$location->state");
            } else {
                return back()->withErrors("Could Not Find Location.  Please check City State and Zip Code.")
                    ->withInput($request->input);
            }
        }

        if($request->input('q')) {
            $query->setQuery($request->input('q'));
            $query->setQueryDefaultOperator('AND');
            $dismax = $query->getDisMax();
            $dismax->setQueryFields('company position^4 description^0.2');
            $dismax->setPhraseFields('description^2');
            $dismax->setMinimumMatch('-35%');

        }

        //$jobQuery->where('posted',1);

        if( $request->input('fromage') && $request->input('fromage') != "any") {
            $from = Carbon::now()->subDays($request->input('fromage'))->startOfDay()->setTimezone('UTC');
            $query->createFilterQuery('posted_at')->setQuery("posted_at:[" . $from->format("Y-m-d\TH:m:s\Z") . " TO NOW]");
        }

        //$query->setQuery(implode($query_parts," AND "));

        $resultset = $solr->select($query);

        $jobCollection = new Collection();

        foreach($resultset as $document) {
            $job = new JobPosting();

            $job->id             = $document->id;
            $job->description    = $document->description;
            $job->company        = $document->company;
            $job->position       = $document->position;
            $job->location_city  = $document->location_city;
            $job->location_state = $document->location_state;
            $job->location_zip   = $document->location_zip;
            $job->url            = $document->url;
            $job->created_at     = Carbon::createFromFormat('Y-m-d\TH:i:s\Z',$document->created_at,'UTC');
            $job->posted_at     = Carbon::createFromFormat('Y-m-d\TH:i:s\Z',$document->posted_at,'UTC');

            $jobCollection->push($job);
        }

        $paginator = new LengthAwarePaginator($jobCollection,$resultset->getNumFound(),10,$request->input('page',1));
        $paginator->setPath('search');

        return $paginator;
    }

    public function getJobsOld($request)
    {
        $jobQuery = JobPosting::query();

        if($request->input('l')) {
            $locations = Geocode::getPosition($request->input('l'));
        } else {
            $locations = [];
        }

        if(count($locations) == 0) {
            throw new Exception('Could not find location (' . $request->input('l') . ').  Please type in valid {city, state}, {county, state}, {zipcode} or {state} in the USA.');
        }
        if(count($locations) > 1) {
            //? throw error with multiple locations ?
        }

        $location = $locations[0];

        if($location->zipcode || $location->city) { // radius accepted
            if($request->input('r',5) > 0) {
                $jobQuery->distance($request->input('r','5'),$location->latlon);
            } else {
                if($location->zipcode) {
                    $jobQuery->where('location_zipcode',$location->zipcode);
                } else {
                    $jobQuery->where([['location_city',$location->city],['location_state',$location->state]]);
                }
            }
        } else { // search based on text (state or county)
            if($location->county && $location->state) {
                //get all cities in county.
                $county = PositionCounty::where([['county',$location->county],['state', $location->state]])->with('positionCities')->first();

                $jobQuery->whereIn('location_city',$county->positionCities->lists('city'));

            } else if($location->state) {
                $jobQuery->where('location_state',$location->state);
            } else {
                return back()->withErrors("Could Not Find Location.  Please check City State and Zip Code.")
                    ->withInput($request->input);
            }
        }

        if($request->input('q')) {
            $jobQuery->whereRaw("(
                company LIKE '%" . $request->input('q') . "%'
                OR description LIKE '%" . $request->input('q') . "%'
                OR position LIKE '%" . $request->input('q') . "%'
                )");
        }

        $jobQuery->where('posted',1);

        if( $request->input('fromage') && $request->input('fromage') != "any") {
            $jobQuery->where('created_at', '>', Carbon::now()->subDays($request->input('fromage')));
        }

        $jobs = $jobQuery->orderBy('job_source_id','asc')->orderBy('created_at','desc')->paginate(10);

        return $jobs;
    }
}
