<?php

namespace App\Http\Controllers;

use App\Jobs\ResumeToImage;
use App\Models\UserResume;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use Image;

use App\Http\Requests;
use Symfony\Component\HttpFoundation\File\MimeType\MimeTypeGuesser;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ResumeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return view('pages.user.resumes.index', ['resumes' => $request->user()->resumes]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.user.resumes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->file('resume-file')) {
            // Need to change read access so that clamAV can read file.
            chmod($request->file('resume-file')->getRealPath(), 0644);
        }

        $this->validate($request,[
            'resume-name' => 'bail|required|unique:user_resumes,name,NULL,id,user_id,' . $request->user()->id,
            'resume-file' => 'bail|required|mimes:doc,docx,pdf|clamav',
        ],
        [
            'resume-name.unique' => 'You already have a resume with that name.',
        ]);

        $resume                   = new UserResume();
        $resume->name             = $request->input('resume-name');
        $resume->default          = $request->input('default')?1:0;
        $resume->user_file_name   = $request->file('resume-file')->getClientOriginalName();
        $resume->system_file_name = $this->moveResumeFile($request->file('resume-file'),$resume);
        $resume->user_id          = $request->user()->id;

        //Set other resume default to 0
        if($resume->default == 1) {
            UserResume::where('user_id', $request->user()->id)->update(['default' => 0]);
        }

        $resume->save();

        return view('pages.user.resumes.index', ['resumes' => $request->user()->resumes]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $resume = UserResume::findOrFail($id);
        if($resume->user_id == $request->user()->id) {
            UserResume::where('user_id', $request->user()->id)->update(['default' => 0]);
            $resume->default = 1;
            $resume->save();

            return ['success' => true];
        } else {
            abort(403, "Unauthorized Action.");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $resume = UserResume::find($id);

        if($resume->user_id == $request->user()->id) {
            $this->removeResumeFile($resume);
            return UserResume::destroy($id);
        } else {
            abort(403, "Unauthorized Action.");
        }
    }

    public function download($systemFileName)
    {
        $resume = UserResume::where('system_file_name',$systemFileName)->first();

        if($resume) {
            $fileContents = Storage::get($resume->getSystemPath());
            $guesser = MimeTypeGuesser::getInstance();

            $mimeType = $guesser->guess(storage_path() . "/app/" . $resume->getSystemPath());

            return response($fileContents,200,['Content-Type' => $mimeType]);
        }

    }

    private function moveResumeFile(UploadedFile $file, UserResume $resume)
    {
        $resume->system_file_name = $resume->getNewSystemFileName($file->getClientOriginalExtension());

        Storage::disk('local')->put($resume->getResumeFolder() . "/" . $resume->system_file_name, File::get($file));

        return $resume->system_file_name;
    }

    private function removeResumeFile(UserResume $resume)
    {
        Storage::disk('local')->delete($resume->getResumeFolder() . "/" . $resume->system_file_name);
    }

}
