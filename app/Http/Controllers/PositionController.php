<?php

namespace App\Http\Controllers;

use App\Models\PositionCity;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Cache;

class PositionController extends Controller
{
    public function ajaxGetCityState(Request $request)
    {
        $response  = [];

        if(Cache::has('position_' . strtoupper($request->input("term")))) {
            return Cache::get('position_' . strtoupper($request->input("term")));
        }

        $positions =  PositionCity::select('city','state')->distinct()->where('city', 'LIKE', strtoupper($request->input("term")) . "%")->orderBy('population','desc')->limit(12)->get();

        foreach($positions as $position) {
            $response[] = ['value' => ucwords(strtolower($position->city)) . ", " . $position->state,
                           'data'  => ucwords(strtolower($position->city)) . ", " . $position->state];
        }

        Cache::put('position_' . strtoupper($request->input("term")),$response,60);

        return $response;
    }
}
