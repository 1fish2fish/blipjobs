<?php

namespace App\Http\Controllers;

use App\Blip\Geocode;
use App\Blip\GoogleMaps;
use App\Blip\UsStates;
use App\Models\JobPosition;
use App\Models\JobPosting;
use App\Models\JobPostingExperienceReq;
use App\Models\JobPostingLanguageReq;
use App\Models\JobPostingLicenseReq;
use App\Models\JobSource;
use App\Models\PositionZipcode;
use App\Models\UserJobPostingStat;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\DB;

class JobPostingController extends Controller
{
    public function admin(Request $request)
    {
        $jobPostings = JobPosting::where("user_id","=",$request->user()->id)->with('jobApplicationStat')->with('jobViewStat')->paginate(5);

        $stats = [

            'job_posts_7' => 0,
            'job_posts'   => 0,
            'job_views_7' => 0,
            'job_views'   => 0,
            'job_apps_7'  => 0,
            'job_apps'    => 0,
        ];

        if($request->user()->hasRole('super-admin')) {
            $stats = [
                'job_posts_7' => UserJobPostingStat::sum('job_posts_7_day'),
                'job_posts'   => UserJobPostingStat::sum('job_posts_today'),
                'job_views_7' => UserJobPostingStat::sum('job_views_7_day'),
                'job_views'   => UserJobPostingStat::sum('job_views_today'),
                'job_apps_7'  => UserJobPostingStat::sum('job_apps_7_day'),
                'job_apps'    => UserJobPostingStat::sum('job_apps_today'),
            ];
        } else {
            if($request->user()->userJobPostingStat) {

                $stats = [
                    'job_posts_7' => $request->user()->userJobPostingStat->job_posts_7_day,
                    'job_posts'   => $request->user()->userJobPostingStat->job_posts_today,
                    'job_views_7' => $request->user()->userJobPostingStat->job_views_7_day,
                    'job_views'   => $request->user()->userJobPostingStat->job_views_today,
                    'job_apps_7'  => $request->user()->userJobPostingStat->job_apps_7_day,
                    'job_apps'    => $request->user()->userJobPostingStat->job_apps_today,
            ];
            }
        }


        return view("pages.job.admin",["jobPostings" => $jobPostings, "stats" => $stats]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $orderBy = [
            'company'    => 'asc',
            'position'   => 'asc',
            'created_at' => 'desc'
        ];

        $jobQuery = JobPosting::query();

        if($request->input('l')) {
            $locations = Geocode::getPosition($request->input('l'));

            $location = $locations[0];

            if ($location->zipcode || $location->city) { // radius accepted
                if ($request->input('r', 5) > 0) {
                    $jobQuery->distance($request->input('r', '5'), $location->latlon);
                } else {
                    if ($location->zipcode) {
                        $jobQuery->where('location_zipcode', $location->zipcode);
                    } else {
                        $jobQuery->where([['location_city', $location->city], ['location_state', $location->state]]);
                    }
                }
            } else { // search based on text (state or county)
                if ($location->county && $location->state) {
                    //get all cities in county.
                    $county = PositionCounty::where([['county', $location->county], ['state', $location->state]])->with('positionCities')->first();

                    $jobQuery->whereIn('location_city', $county->positionCities->lists('city'));

                } else if ($location->state) {
                    $jobQuery->where('location_state', $location->state);
                } else {
                    $error = 'Could not find location (' . $request->input('l') . ').  Please type in valid {city, state}, {county, state}, {zipcode} or {state} in the USA.';
                    return view('pages.search.list', [
                        'jobs' => new Collection(),
                        'input' => $request->input()])->withErrors($error);
                }
            }
        }

        if($request->input('q')) {
            $jobQuery->whereRaw("(
                company LIKE '%" . $request->input('q') . "%'
                OR description LIKE '%" . $request->input('q') . "%'
                OR position LIKE '%" . $request->input('q') . "%'
                )");
        }

        if($request->input('source')) {
            $jobQuery->where('job_source_id', $request->input('source'));
        }

        if($request->input('published','all') == 'yes'){
            $jobQuery->where('posted',1);
        } else if($request->input('published','all') == 'no') {
            $jobQuery->where('posted',0);
        }

        if( $request->input('fromage') && $request->input('fromage') != "any") {
            $jobQuery->where('created_at', '>', Carbon::now()->subDays($request->input('fromage')));
        }

        if(!$request->user()->hasRole('super-admin')) {
            $jobQuery->where("user_id","=",$request->user()->id);
        }

        $jobs = $jobQuery->orderBy($request->input('order_by','created_at'), $orderBy[$request->input('order_by','created_at')])->paginate(10);

        return view("pages.job.index",["jobPostings" => $jobs, 'input' => $request->input()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("pages.job.create", ['states' => UsStates::getStates()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'company'  => 'required|min:3',
            'position' => 'required|min:3',
            'location_city'    => 'required|min:2',
            'location_zipcode' => 'required|min:501|numeric',
            'description'      => 'required|min:50',
            'contact'          => 'required|email'
        ]);

        if(! $geoPosition = $this->getGeoPosition($request->input('location_city'),$request->input('location_state'),$request->input('location_zipcode'))) {
            return back()->withErrors("Could Not Find Location.  Please check City State and Zip Code.")
                ->withInput($request->input);
        }

        //there should only be one position added with the same company, position, location, user per day.
        $source_key = sha1($request->input('company') . $request->input('position') .
            $request->input('location_city') . $request->input('location_state') . $request->input('location_zipcode') .
            $request->user()->id . date("Y-m-d"));



        $jobPosting = new JobPosting();

        $jobPosting->company       = $request->input('company');
        $jobPosting->position      = $request->input('position');
        $jobPosting->salary        = $request->input('salary');
        $jobPosting->salary_per    = $request->input('salary_per');
        $jobPosting->education     = $request->input('education');
        $jobPosting->description   = $request->input('description');
        $jobPosting->location_latlon  = $geoPosition->latlon;
        $jobPosting->location_city    = $request->input('location_city');
        $jobPosting->location_state   = $request->input('location_state');
        $jobPosting->location_zip     = $request->input('location_zipcode');
        $jobPosting->contact          = $request->input('contact');
        $jobPosting->user_id          = $request->user()->id;
        $jobPosting->job_source_id    = JobSource::where('name','BlipJobs')->first()->id;
        $jobPosting->setJobSourceKey();

        $dupeJob = JobPosting::where("job_source_key",$jobPosting->job_source_key )->get();
        if($dupeJob->count() > 0) {
            return back()->withErrors("There is already a job like this in the database.")
                ->withInput($request->input);
        }

        $jobPosting->save();

        $jobPosition                  = JobPosition::where('blip_name', $request->input('position_type'))->first();

        if($jobPosition->count() == 0) {
            $jobPosting->delete();
            return back()->withErrors("Job Position Type Not Found.")
                ->withInput($request->input);
        }

        DB::statement("INSERT INTO job_position_job_posting
                        (job_posting_id, job_position_id)
                        VALUES
                        ({$jobPosting->id},{$jobPosition->id})");

        return view('pages.job.requirements.create', ['jobPosting' => $jobPosting]);

    }

    private function getGeoPosition($city, $state, $zip)
    {
        if($position = PositionZipcode::where("zipcode", "=", $zip)->first()) {
            return $position;
        }//else zip is not in the database.  get from google.

        $gMaps    = new GoogleMaps(env("GOOGLE_MAPS_API"));
        $positions = $gMaps->geocode("$city, $state $zip");

        if(count($positions) == 1) { // if count is 0 or more than 1 it isn't an exact match.

            if(strpos("$city, $state $zip", $positions[0]['address']) !== false) {
                $p         = new PositionZipcode();
                $p->latlon = $positions[0]['lat'] . "," . $positions[0]['lon'];

                //TODO: should update zipcode table if results found.
                return $p;
            }
        }

        return false;

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $jobPosting = JobPosting::findOrFail($id);

        return view('pages.job.show', ['jobPosting' => $jobPosting]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $jobPosting = JobPosting::findOrFail($id);

        if($request->user()->id != $jobPosting->user_id) {
            return abort(401, "Unauthorized");
        }

        return view('pages.job.edit', ['jobPosting' => $jobPosting, 'states' => UsStates::getStates()]);
    }

    public function editRequirements(Request $request, $id)
    {
        $jobPosting = JobPosting::findOrFail($id);

        if($request->user()->id != $jobPosting->user_id) {
            return abort(401, "Unauthorized");
        }

        return view('pages.job.requirements.edit', ['jobPosting' => $jobPosting]);
    }

    public function createRequirements(Request $request, $id)
    {
        $jobPosting = JobPosting::find($id);

        if($request->user()->id != $jobPosting->user_id) {
            return abort(401, "Unauthorized");
        }

        if($jobPosting && $jobPosting->user->id == $request->user()->id) {
            return view('pages.job.requirements.create',['jobPost' => $jobPosting]);
        }
    }

    public function storeRequirements(Request $request, $id)
    {
        $jobPosting = JobPosting::findOrFail($id);

        if($request->user()->id != $jobPosting->user_id) {
            return abort(401, "Unauthorized");
        }

        $this->saveRequirements($request, $id);

        return redirect()->action('JobPostingController@index');
    }


    private function saveRequirements($request, $id) {
        foreach($request->input('experience') as $experience) {

            if($experience["description"]) {
                $experienceReq = new JobPostingExperienceReq();

                $experienceReq->job_posting_id = $id;
                $experienceReq->experience     = $experience["description"];
                $experienceReq->years          = $experience["years"];

                $experienceReq->save();
            }
        }

        foreach($request->input('language') as $language) {

            if($language) {
                $l = new JobPostingLanguageReq();

                $l->job_posting_id = $id;
                $l->language = $language;

                $l->save();
            }
        }

        foreach($request->input('license') as $license) {

            if($license) {
                $l = new JobPostingLicenseReq();

                $l->job_posting_id = $id;
                $l->license        = $license;

                $l->save();
            }
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'company'  => 'required|min:3',
            'position' => 'required|min:3',
            'description' => 'required|min:50',
        ]);


        $jobPosting = JobPosting::findOrFail($id);

        if($request->user()->id != $jobPosting->user_id) {
            return abort(401, "Unauthorized");
        }

        $jobPosting->company       = $request->input('company');
        $jobPosting->position      = $request->input('position');
        $jobPosting->contact       = $request->input('contact');
        $jobPosting->salary        = $request->input('salary');
        $jobPosting->salary_per    = $request->input('salary_per');
        $jobPosting->education     = $request->input('education');
        $jobPosting->description   = $request->input('description');

        $jobPosting->save();

        $jobPosition                  = JobPosition::where('blip_name', $request->input('position_type'))->first();

        if($jobPosition->count() == 0) {
            return back()->withErrors("Job Position Type Not Found.")
                ->withInput($request->input);
        }

        $jobPosting->jobPositions()->sync([$jobPosition->id]);

        return redirect()
            ->action('JobPostingController@index',$jobPosting->id)
            ->with('success', 'Job Posting Updated.');
    }


    public function updateRequirements(Request $request, $id)
    {
        $jobPosting = JobPosting::findOrFail($id);

        if($request->user()->id != $jobPosting->user_id) {
            return abort(401, "Unauthorized");
        }

        JobPostingExperienceReq::where("job_posting_id", "=", $id)->delete();
        JobPostingLanguageReq::where("job_posting_id", "=", $id)->delete();
        JobPostingLicenseReq::where("job_posting_id", "=", $id)->delete();

        $this->saveRequirements($request, $id);

        return redirect()->action('JobPostingController@index')->with('success','Job Posting Updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $jobPosting = JobPosting::findOrFail($id);

        if($request->user()->id != $jobPosting->user_id) {
            return abort(401, "Unauthorized");
        }

        return ['success' => $jobPosting->delete()];
    }

    public function publish(Request $request) {
        $jobPosting = JobPosting::findOrFail($request->input('id'));

        $jobPosting->posted = 1;
        $jobPosting->save();

        return ['success'];
    }

    public function unpublish(Request $request) {
        $jobPosting = JobPosting::findOrFail($request->input('id'));

        $jobPosting->posted = 0;
        $jobPosting->save();

        return ['success'];
    }
}
